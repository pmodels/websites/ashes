<h1>Important Dates</h1>
<div id="description">
  <p><b><strike>Paper Submission: Jan. 08, 2016 <a href="http://en.wikipedia.org/wiki/Anywhere_on_Earth">AoE</a></strike></b></p>
  <p><b><strike>DEADLINE EXTENDED - Paper Submission: Jan. 15, 2016 <a href="http://en.wikipedia.org/wiki/Anywhere_on_Earth">AoE</a></strike></b></p>
  <p><b>FINAL DEADLINE EXTENSION - Paper Submission: Jan. 20, 2016 <a href="http://en.wikipedia.org/wiki/Anywhere_on_Earth">AoE</a></b></p>
  <p><b>Paper Notification:</b> Feb. 18, 2016</p>
  <p><b>Camera-Ready Papers Due:</b> Feb. 25, 2016</p>
</div>

