<html>
  <head>
    <title>AsHES Workshop: AsHES 2016 Preliminary Program</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">
      <?php include('header.php'); ?>

<div id="sub-frame">
	<div class="midBox1">
	<h1>Opening remarks</h1>
	<h3>8:30 - 8:45 am<br />
	AsHES General Chair: James Dinan, Intel, USA</h3>[ <a href="slides/session1/Jim_AsHES2016OpeningRemarks.pdf">slides</a> ]
	</div>

<div class="midBox1">
<h1> Keynote by <font color="#16A085"><b> <a href="https://www.ece.illinois.edu/directory/profile/w-hwu">Professor Wen-mei W. Hwu</a></b></font></h1>[ <a href="slides/session1/Hwu_AsHES-Keynote-Hwu-2016.pdf">slides</a> ] 
<h3>8:45 - 9:50 am</h3>

<h4>"Addressing the Accelerator Programming Challenges in Exascale Systems"
</h4>
<h4><b>Abstract:</b>
<font color="#16A085"><img src="pics/Wen-mei.jpg" border="3" align="right" class="right"/></font> 
Since the introduction of CUDA in 2006, we have made tremendous progress in heterogeneous supercomputing. We have built heterogeneous top-ranked supercomputers. Much has been learned about of algorithms, languages, compilers and hardware architecture in this movement. What is the benefit that science teams are seeing? How hard is it to program these systems today? How will we programming these systems in the future? In this talk, I will go over the lessons learned from educating programmers, migrating Blue Waters applications into GPUs, and developing performance-critical libraries. Iwill then give a preview of the types of programming systems that will be needed to further reduce the software cost of heterogeneous computing.
</h4>

<h4><b>Bio:</b>
Wen-mei W. Hwu is the Walter J. ("Jerry") Sanders III-Advanced Micro Devices Endowed Chair in Electrical and Computer Engineering in the Coordinated Science Laboratory of the University of Illinois at Urbana-Champaign. Dr. Hwu received his Ph.D. degree in Computer Science from the University of California, Berkeley, 1987.
<br />
His research interests are in the areas of architecture,implementation,software for high-performance computer systems, and parallel processing. He is a Principal Investigator(PI) for the petascale Blue Waters system, is co-director of the Intel and Microsoft funded Universal Parallel Computing Research Center (UPCRC), and PI for the world's first NVIDIA GPU Center of Excellence. At the Illinois Coordinated Science Lab,he is the director of the OpenIMPACT project, which has delivered new compiler and computer architecture technologies to the computer industry since 1987. He also serves as the Soft Systems Theme leader of the MARCO/DARPA Gigascale Silicon Research Center (GSRC) and on the Executive Committees of both the GSRC and the MARCO/DARPA Center for Circuit and System Solutions (C2S2).
<br />
For his contributions to the areas of compiler optimization and computer architecture, he received the 1993 Eta Kappa Nu Outstanding Young Electrical Engineer Award, the 1994 Xerox Award for Faculty Research, the 1994 University Scholar Award of the University of Illinois, the 1997 Eta Kappa Nu Holmes MacDonald Outstanding Teaching Award, the 1998 ACM SigArch Maurice Wilkes Award, the 1999 ACM Grace Murray Hopper Award, the 2001 Tau Beta Pi Daniel C. Drucker Eminent Faculty Award, and the 2002 ComputerWorld Honors Archive Medal. He is a fellow of IEEE and of the ACM. 
<br />
From 1997 to 1999, Prof. Hwu served as chairman of the Computer Engineering Program at the University of Illinois. In 2007 he introduced a new engineering course in massively parallel processing, which he co-taught with David Kirk, Chief Scientist of NVIDIA. In 2008, he was named co-director of one of two Universal Parallel Computing Research Centers sponsored by Microsoft and Intel.

</h4>
<!--
<h4><b>Bio:</b>
Michela Taufer is the David L. and Beverly J.C. Mills Chair of Computer and 
Information Sciences and an associate professor in the same department at the 
University of Delaware.  She earned her master’s degrees in Computer 
Engineering from the University of Padova (Italy) and her doctoral degree in 
Computer Science from the Swiss Federal Institute of Technology (Switzerland).  
From 2003 to 2004 she was a La Jolla Interfaces in Science Training Program 
(LJIS) Postdoctoral Fellow at the University of California San Diego (UCSD) and 
The Scripps Research Institute (TSRI), where she worked on interdisciplinary 
projects in computer systems and computational chemistry.  From 2005 to 2007, 
she was an Assistant Professor at the Computer Science Department of the 
University of Texas at El Paso (UTEP).  She joined the University of Delaware 
in 2007 as an Assistant Professor and was promoted to Associate Professor with 
tenure in 2012.
<br />
Taufer's research interests include scientific applications and their advanced 
programmability in heterogeneous computing (i.e., multi-core and many-core 
platforms, GPUs); performance analysis, modeling, and optimization of 
multi-scale applications on heterogeneous computing, cloud computing, and 
volunteer computing; numerical reproducibility and stability of large-scale 
simulations on multi-core platforms; big data analytics and MapReduce.
</h4> -->
</div>

<div class="midBox1">
<h2>Break 9:50 - 10:20 am</h2>
</div>

<div class="midBox1">
<h1>Session 1: Programming Models and Tools</h1>
<h3>10:20 am - 12:00 pm<br />
Session Chair: Sandra Catalán, Universitat Jaume I, Castellón, Spain
</h3>
<ul>
    <li>
    <b>Heterogeneous Streaming</b><br />
    Cj Newburn, Gaurav Bansal, Michael Wood, Luis Crivelli, Judit Planas, Alejandro Duran, Paulo Souza, Leonardo Borges, Piotr Luszczek, Stanimire Tomov, Jack Dongarra, Hartwig Anzt, Mark Gates, Azzam Haidar, Yulu Jia, Khairul Kabir, Ichitaro Yamazaki and Jesus Labarta [ <a href="slides/session1/Newburn_Heterogeneous Streaming-IPDPS-AsHES16.pdf">slides</a> ]
    </li>
    <li>
    <b>HMC-Sim-2.0: A Simulation Platform for Exploring Custom Memory Cube Operations</b><br />
    John Leidel and Yong Chen [ <a href="slides/session1/ashes2016-LEIDEL.pdf">slides</a> ]
    </li>
    <li>
    <b>Alpaka – An Abstraction Library for Parallel Kernel Acceleration</b><br />
    Erik Zenker, Benjamin Worpitz, René Widera, Axel Huebl, Guido Juckeland, Wolfgang E. Nagel, Michael Bussman and Andreas Knüpfer [ <a href="slides/session1/ashes16_alpaka.pdf">slides</a> ]
    </li>
    <li>
    <b>A Tool for Bottleneck Analysis and Performance Prediction for GPU-accelerated applications</b><br />
    Souley Madougou, Ana Lucia Varbanescu, Cees De Laat and Rob Van Nieuwpoort [ <a href="slides/session1/S_Maduo_ashes2016.pdf">slides</a> ]
    </li>
</ul>
</div>

<div class="midBox1">
<h2>Lunch 12:00 - 1:30 pm</h2>
</div>

<div class="midBox1">
<h1>Session 2: Algorithms and Applications</h1>
<h3>1:30 - 3:10 pm<br />
Session Chair: CJ Newburn, Intel, USA
</h3>
<ul>
    <li>
    <b>Hessenberg Reduction with Transient Error Resilience on GPU-Based Hybrid Architectures</b><br />
    Yulu Jia, Piotr Luszczek and Jack Dongarra
    </li>
    <li>
    <b>Optimization of Block Sparse Matrix-Vector Multiplication on Shared-Memory Architectures</b><br />
    Ryan Eberhardt and Mark Hoemmen [ <a href="slides/session2/Ryan_Eberhardt_slides.pdf">slides</a> ]
    </li>
    <li>
    <b>Basker: A Threaded Sparse LU Factorization Utilizing Hierarchical Parallelism and Data Layouts</b><br />
    Joshua Booth, Sivasankaran Rajamanickam and Heidi Thornquist[ <a href="slides/session2/_Basker_IPDPS_2016.pdf">slides</a> ]
    </li>
    <li>
    <b>Efficiency of general Krylov methods on GPUs – An experimental study</b><br />
    Hartwig Anzt, Jack Dongarra, Moritz Kreutzer, Gerhard Wellein and Martin Koehler [ <a href="slides/session2/H_anzt_ashes2016_ft_hrd.pdf">slides-1</a> ][ <a href="slides/session2/krylov_study_ashes.pdf">slides-2</a> ]
    </li>   
</ul>
</div>

<div class="midBox1">
<h2>Break 3:10 - 3:40 pm</h2>
</div>

<div class="midBox1">
<h1>Session 3: Workload Scheduling</h1>
<h3>3:40 - 4:55 pm<br />
Session Chair: Piotr Luszczek, University of Tennessee, Knoxville, USA
</h3>
<ul>
    <li>
    <b>Refactoring Conventional Task Schedulers to Exploit Asymmetric ARM big.LITTLE Architectures in Dense Linear Algebra</b><br />
    Luis Costero, Katzalin Olcoz, Francisco D. Igual, Sandra Catalán, Rafael Rodríguez-Sánchez and Enrique S. Quintana-Ortí [ <a href="slides/session3/Sandra_Presentacion.pdf">slides</a> ]
    </li>
    <li>
    <b>Heterogeneous CAF-based Load Balancing on Intel Xeon Phi</b><br />
    Valeria Cardellini, Alessandro Fanfarillo and Salvatore Filippone [ <a href="slides/session3/AsHES_Fanfarillo.pdf">slides</a> ]
    </li>
    <li>
    <b>Topology-Aware GPU Selection on Multi-GPU Nodes</b><br />
	<b> BEST PAPER AWARD WINNER </b>
    Iman Faraji, Seyed Hessam Mirsadeghi and Ahmad Afsahi [ <a href="slides/session3/AsHES2016_seyed.pdf">slides</a> ]
    </li>

</ul>
</div>

	<div class="midBox1">
	<h1>Closing Remarks</h1>
	<h3>4:55 - 5:00 pm</h3>
	</div>

</div>
	<?php include('footer.php'); ?>
</div>

</body>
</html>
