<html>
  <head>
    <title>AsHES Workshop</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">

      <?php include('header.php'); ?>
      <div id="sub-frame">

        <h1>Steering Committee</h1>
        <div id="description">
          <p><b><a href="http://www.mcs.anl.gov/~balaji">Pavan Balaji</a></b>, Argonne National Laboratory, USA</p>
          <p><b><a href="http://www.rdcps.ac.cn/~zyq/zyq-en.html">Yunquan Zhang</a></b>, Chinese Academy of Sciences, China</p>
          <p><b><a href="http://matsu-www.is.titech.ac.jp/~matsu/">Satoshi Matsuoka</a></b>, Tokyo Institute of Technology, Japan</p>
          <p><b><a href="http://www.cs.virginia.edu/~jm6dg/">Jiayuan Meng</a></b>, Argonne National Laboratory, USA</p>
          <p><b><a href="http://qcri.org.qa/page?a=117&name=Xiaosong_Ma&pid=154&lang=en-CA">Xiaosong Ma</a></b>, Qatar Computing Research Institute, Qatar</p>
          <p><b><a href="http://www2.cs.uh.edu/~chapman/">Barbara Chapman</a></b>, University of Houston, USA</p>
          <p><b><a href="http://www.capsl.udel.edu/~ggao/">Guang R. Gao</a></b>, University of Delaware, USA</p>
          <p><b>Xinmin Tian</b>, Intel, USA</p>
          <p><b>Michael Wong</b>, IBM, Canada</p>
        </div>

        <h1>General Chair</h1>
        <div id="description">
          <p><b><a href="http://scholar.google.com/citations?user=imdGgDAAAAAJ&hl=en">James Dinan</a></b>, Intel Corporation</p>
        </div>

        <h1>Program Co-Chairs</h1>
        <div id="description">
          <p><b><a href="http://hpc.cs.tsinghua.edu.cn/research/cluster/cwg.html">Wenguang Chen</a></b>, Tsinghua University, China</p>
          <p><b><a href= "http://sunisg123.wix.com/sunitachandra">Sunita Chandrasekaran</a></b>, University of Delaware, USA</p>
          <p><b><a href = "http://www.bsc.es/about-bsc/staff-directory/pena-antonio">Antonio J. Pe&ntilde;a</a></b>, Barcelona Supercomputing Center, Spain</p>
        </div>

        <h1>Publicity Chair</h1>
        <div id="description">
          <p><b>Rezaul Karim Raju</b>, University of Houston, USA</p>
        </div>

        
        <h1>Technical Program Committee</h1>
        <div id="description">
        
            <p><b>Sangmin Seo</b>, Argonne National Laboratory, USA</p>
            <p><b>Piotr Luszczek</b>, University of Tennessee Knoxville, USA</p>
            <p><b>Anthony Danalis</b>, University of Tennessee, USA</p>
            <p><b>Gabriele Jost</b>, Intel Corporation, USA</p>
            <p><b>Jeff Hammond</b>, Intel Labs, USA</p>
            <p><b>Seyong Lee</b>, Oak Ridge National Laboratories, USA</p>
            <p><b>John Lidel</b>, Texas Tech University, USA</p>
            <p><b>James Beyer</b>, NVIDIA Corporation, USA</p>
            <p><b>Kamesh Madduri</b>, The Pennsylvania State University, USA</p>
            <p><b>Mahantesh M Halappanavar</b>, Pacific Northwest National Laboratory, USA</p>
            <p><b>Stephen Olivier</b>,  Sandia Nationl Lab, USA</p>
            <p><b>Guido Juckeland</b>, TU Dresden, Germany </p>
            <p><b>Matthias Muller</b>, TU Aachen, Germany</p>
			<p><b>Barry Rountree</b>, Lawrence Livermore National Laboratory, USA</p>
			<p><b>Hennry Jin</b>, NASA, USA</p>
			<p><b>Dong Li</b>, University of Calfornia, Merced, USA</p>
            <p><b>Khaled Hamidouche</b>, The Ohio State University, USA<p>
			<p><b>Huimin Cui</b>, Institute of Computing Technology, CAS</p>
			<p><b>Xipeng Shen</b>, North Carolina State University, USA<p>
			<p><b>Bronis de Supinski</b>,  Lawrence Livermore National Laboratory, USA</p>
			<p><b>Hao Wang</b>, Virginia Tech, USA</p>
			<p><b>Naoya Maruyama</b>, RIKEN AICS</p>
			<p><b>Siva Kumar Sastry Hari</b>, NVIDIA Corporation, USA<p>
			<p><b>Guangyu Sun</b>, Peking University, China </p>
			<p><b>Sriram Krishnamoorthy</b>,  Pacific Northwest National Laboratory, USA</p>
			<p><b>Nacho Navarro</b>,  UPC- Univesity Politecnica de Catalunya, Spain</p>
			<p><b>Kelly Shaw</b>, University of Richmond, USA<p>
			<p><b>Yongpeng Zhang</b>, Stone Ridge Technology, USA<p>
			<p><b>Fangfang Xia</b>, Argonne National Laboratory, USA<p>
			
			
        </div>

        <h1>Journal Special Issue Co-editors</h1>
        <div id="description">
          <p><b><a href= "http://sunisg123.wix.com/sunitachandra">Sunita Chandrasekaran</a></b>, University of Delaware, USA</p>
          <p><b><a href = "http://www.bsc.es/about-bsc/staff-directory/pena-antonio">Antonio J. Pe&ntilde;a</a></b>, Barcelona Supercomputing Center, Spain</p>
        </div>

      </div>

      <?php include('footer.php'); ?>

    </div>
  </body>
</html>
