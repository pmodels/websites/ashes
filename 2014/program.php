<html>
  <head>
    <title>AsHES Workshop</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">

      <?php include('header.php'); ?>
	</ul>

<div class="midBox1">
<h1>Opening remarks</h1>
<h3>8:30 - 8:45 am</h3>
</div>

<div class="midBox1">
<h1>Keynote by Jeffrey Vetter: Exploring Emerging Technologies in the HPC Co-Design Space</h1>
<h3>8:45 - 9:50 am</h3>   <a href="slides/ashes14-vetter.pdf">Slides</a>
<h4><b>Abstract:</b> Concerns about energy-efficiency and reliability have forced our community to reexamine the full spectrum of architectures, software, and algorithms that constitute our ecosystem. While architectures and programming models remained relatively stable for almost two decades, new architectural features, such as heterogeneous processing, nonvolatile memory, and optical interconnection networks, will demand that applications be redesigned so that they expose massive amounts of hierarchical parallelism, carefully orchestrate data movement, and balance concerns over accuracy, reliability, and time to solution. In what we have termed 'co-design,' teams of architects, software designers, and applications scientists, are working collectively to realize an integrated solution to these challenges. Not surprisingly, this design space can be massive and uncertain. To assist in this design space exploration, our team is using modeling, simulation, and measurement on prototype systems in order to assess the possible trajectories of these future systems. In this talk, I will sample these emerging technologies and discuss how we are preparing for these upcoming systems.</h4>
</div>

<div class="midBox1">
<h2>Break 9:50 - 10:20 am</h2>
</div>

<div class="midBox1">
<h1>Session 1: Programing Model and Performance Optimizations (Chair: Karthikeyan Vaidyanathan, Intel)</h1>
<h3>10:20 am - 12:00 pm</h3>
<ul>
	<li>
	<b>Scalable Critical Path Analysis for Hybrid MPI-CUDA Applications</b> </br>
	Felix Schmitt, Robert Dietrich and Guido Juckeland  <a href="slides/ashes14-schmitt.pdf">Slides</a>
        </li>
	<li>
	<b>Dymaxion++: a Directive-Based API to Optimize Data Layout and Memory Mapping for Heterogeneous Systems</b> </br>
	Shuai Che, Jiayuan Meng and Kevin Skadron
        </li>
	<li>
	<b>Comparison of Parallel Programming Models on Intel MIC Computer Cluster</b> </br>
	Chenggang Lai, Zhijun Hao, Miaoqing Huang, Xuan Shi and Haihang You <a href="slides/ashes14-lai.pdf">Slides</a>
        </li>
	<li>
	<b>CoAdELL: Adaptivity and Compression for Improving Sparse Matrix-Vector Multiplication on GPUs</b> </br>
	Marco Maggioni and Tanya Berger-Wolf  <a href="slides/ashes14-maggioni.pdf">Slides</a>
        </li>
        <br>
	</ul>
</div>

<div class="midBox1">
<h2>Lunch 12:00 - 1:30 pm</h2>
</div>

<div class="midBox1">
<h1>Session 2: Accelerating Applications (Chair: Sunita Chandrasekaran, University of Houston)</h1>
<h3>1:30 - 3:10 pm</h3>
<ul>
	<li>
	<b>Optimizing Krylov Subspace Solvers on Graphics Processing Units</b> </br>
        Hartwig Anzt, William Sawyer, Stanimire Tomov, Piotr Luszczek, Ichitaro Yamazaki and Jack Dongarra  <a href="slides/ashes14-anzt.pdf">Slides</a>
        </li>
	<li>
	<b>XSW: Accelerating Biological Database Search on Xeon Phi</b> </br>
	Lipeng Wang, Yuandong Chan, Xiaohui Duan, Haidong Lan, Xiangxu Meng and Weiguo Liu  <a href="slides/ashes14-wang.pdf">Slides</a>
        </li>
	<li>
	<b>Dynamically balanced synchronization-avoiding LU factorization with multicore and GPUs</b> </br>
	Simplice Donfack, Stanimire Tomov and Jack Dongarra <a href="slides/ashes14-donfack.pdf">Slides</a>
        </li>
	<li>
	<b>Scalable Fast Multipole Accelerated Vortex Methods</b> </br>
	Qi Hu, Nail Gumerov, Rio Yokota, Lorena Barba and Ramani Duraiswamii <a href="slides/ashes14-hu.pptx">Slides</a>
        </li>
        <br>
	</ul>
</div>

<div class="midBox1">
<h2>Break 3:10 - 3:40 pm</h2>
</div>

<div class="midBox1">
<h1>Session 3: Emerging Hybrid Systems (Chair: Michela Taufer, University of Delaware)</h1>
<h3>3:40 - 4:55 pm</h3>
<ul>
	<li>
	<b>Infiniband-Verbs on GPU: A case study of controlling an Infiniband network device from the GPU</b> </br>
        Lena Oden and Holger Fr&ouml;ning <a href="slides/ashes14-oden.pdf">Slides</a>
        </li>
	<li>
	<b>Programming the Adapteva Epiphany 64-core Network-on-chip Coprocessor</b> </br>
	Anish Varghese, Robert Edwards, Gaurav Mitra and Alistair Rendell  <a href="slides/ashes14-varghese.pdf">Slides</a>
        </li>
	<li>
	<b>High-Performance Zonal Histogramming on Large-Scale Geospatial Rasters Using GPUs and GPU-Accelerated Clusters</b> </br>
	Jianting Zhang and Dali Wang
        </li>
        <br>
	</ul>
</div>


    </div>

<?php include('footer.php'); ?>
</body>
</html>
