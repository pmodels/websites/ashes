<html>
  <head>
    <title>AsHES Workshop</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">

      <?php include('header.php'); ?>
      <div id="sub-frame">

		<h1>Steering Committee</h1>
        <div id="description">
          <p><b><a href="http://www.mcs.anl.gov/~balaji">Pavan
          Balaji</a></b>, Argonne National Laboratory (co-chair)</p>
          <p><b><a href="http://www.rdcps.ac.cn/~zyq/zyq-en.html">Yunquan
          Zhang</a></b>, Chinese Academy of Sciences</p>
          <p><b><a href="http://matsu-www.is.titech.ac.jp/~matsu/">Satoshi
          Matsuoka</a></b>, Tokyo Institute of Technology</p>
          <p><b><a href="http://www.cs.virginia.edu/~jm6dg/">Jiayuan
          Meng</a></b>, Argonne National Laboratory (co-chair)</p>
        </div>

		<h1>General Chair</h1>
        <div id="description">
          <p><b><a href="http://www.rdcps.ac.cn/~zyq/zyq-en.html">Yunquan
          Zhang</a></b>, Chinese Academy of Sciences</p>
        </div>
          	
		<h1>Program Chairs</h1>
        <div id="description">
          <p><b><a href="http://scholar.google.com/citations?user=imdGgDAAAAAJ&hl=en">James Dinan</a></b>, Intel </p>
          <p><b><a href="http://www4.ncsu.edu/~xma/">Xiaosong
          Ma</a></b>, North Carolina State University</p>
        </div>


        <h1>Publicity Chairs</h1>
        <div id="description">
          <p><b><a href="http://www.alcf.anl.gov/~yaozhang/">Yao Zhang</a></b>, ANL</p>
          <p><b><a href="http://www.mcs.anl.gov/~apenya/">Antonio J. Peña</a></b>, ANL</p>
        </div>

        <h1>Web Chair</h1>
        <div id="description">
          <p><b>Liang Yuan</b>, Chinese Academy of Sciences</p>
        </div>
          	
		<h1>Technical Program Committee</h1>
        <div id="description">
	<p><b>Gabriele Jost</b>, Intel</p>
<p><b>Yongpeng Zhang</b>, Stone Ridge</p>
<p><b>Carl Beckmann</b>, Intel</p>
<p><b>Antonio J. Peña</b>, ANL</p>
<p><b>Fangfang Xia</b>, ANL</p>
<p><b>Naoya Maruyama</b>, Tokyo Tech</p>
<p><b>Sriram Krishnamoorthy</b>, PNNL</p>
<p><b>Bronis de Supinski</b>, LLNL</p>
<p><b>Dong Li</b>, ORNL</p>
<p><b>Surendra Byna</b>, LBNL</p>
<p><b>Dimitris Nikolopoulos</b>, UOC</p>
<p><b>Xipeng Shen</b>, William & Mary</p>
<p><b>Polychronis Xekalakis</b>, Intel</p>
<p><b>Huimin Cui</b>, CAS</p>
<p><b>Siva Hari</b>, NVidia</p>
<p><b>Feng Ji</b>, VMWare</p>
<p><b>Hao Wang</b>, Virginia Tech</p>
<p><b>Rong Ge</b>, Marquette</p>
        </div>
          	
		<h1>Journal Special Issue Co-editors</h1>
        <div id="description">
	 <p><b><a href="http://www.rdcps.ac.cn">Yunquan
          Zhang</a></b>, CAS</p>
	<p><b><a href="http://ft.ornl.gov/~dol/">Dong Li</a></b>, ORNL</p>
          <br><br>
        </div>
          	
      </div>

      <?php include('footer.php'); ?>

    </div>
  </body>
</html>
