<h1>Important Dates</h1>
<div id="description">
  <p><b>Paper Submission:</b> <del>February 1, 2019</del> February 8, 2019 (Friday), <a href="http://en.wikipedia.org/wiki/Anywhere_on_Earth">AoE</a> <font color="red">[FINAL DEADLINE]</font></p>
  <p><b>Paper Notification:</b> <del>March 1, 2019</del> March 3, 2019 (Sunday)</p>

  <p><b>Camera-Ready Papers Due:</b> <del>March 21, 2019</del> March 20, 2019 (Wednesday)</p>
</div>

