
<!--<div id=heading style="background-image: url(pics/head_bkground.png)">-->
<div id="heading">
  <br>
  <table style="width:100%">
    <tr>
      <td width="30%" border="1"></td>
      <td width="60%" align="center">
        <div id="title" align="center">
          The Ninth International Workshop on
          <br>Accelerators and Hybrid Exascale Systems 
          <br> (AsHES)
          <br>
        </div>
        <div id="subtitle" align="center">
          Join us on May 20th, 2019
          <br>Rio de Janeiro, Copacabana, Brazil
          <br>To be held in conjunction with
          <br><font color="#FFF" style="font-weight: bold;" >33rd IEEE International Parallel and Distributed Processing Symposium</font>
        </div>
      </td>
    	<td width=“10%”><div align="left"><a href="http://www.ipdps.org"><img src="pics/IPDPS_2019_logo.jpg" border="1" width="250" height="102"/></a></div></td>
    </tr> 
    <tr valign="bottom"> 
      <td colspan="3"><br/>
        <div id="topnavigation">
          <ul>
            <li><a href="index.php" class="rborder">HOME</a></li>
            <li><a href="organizers.php" class="rborder lborder">ORGANIZERS</a></li>
            <li><a href="cfp.php" class="rborder lborder">CALL FOR PAPERS</a></li>
            <li><a href="reg.php" class="rborder lborder">REGISTRATION</a></li>
            <li><a href="program.php" class="rborder lborder">PROGRAM</a></li>
            <li><a href="submission.php" class="rborder lborder">SUBMISSION</a></li>
            <li><a href="contact.php" class="lborder">CONTACT US</a></li>
          </ul>
        </div>
      </td>
    </tr>
  </table>
</div>

