<html>
  <head>
    <title>AsHES Workshop: AsHES 2019 Preliminary Program</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">
      <?php include('header.php'); ?>

	  <div id="sub-frame">

	  <div class="midBox1">
        <h1>Opening Remarks</h1>
        <h3>8:45 - 9:00 am</h3>
      </div>

      <div class="midBox1">
        <h1>Keynote</h1>
        <h3>9:00 - 10:00 am</h3>
        <h4><b>Performance Portability with Data-Centric Parallel Programming</b></h4>
        <h3>Torsten Hoefler, ETH Zürich, Switzerland</h3>
        <h4><b>Abstract:</b>
        <font color="#16A085"><img src="pics/hoefler_tree_full_style_small.jpg" border="1" align="right" class="right" width="200"/></font>
		The ubiquity of accelerators in high-performance computing has driven programming complexity beyond the skill-set
		of the average domain scientist. To maintain performance portability in the future, it is imperative to decouple
		architecture-specific programming paradigms from the underlying scientific computations. We	present the Stateful
		DataFlow multiGraph (SDFG), a data-centric intermediate representation that enables separating code definition from
		its optimization. We show how to tune several applications in this model and IR. Furthermore, we show a global,
		datacentric view of a state-of-the-art quantum transport simulator to optimize its execution on supercomputers. The
		approach yields coarse and fine-grained data-movement characteristics, which are used for performance and
		communication modeling, communication avoidance, and data-layout transformations. The transformations are tuned
		for the Piz Daint and Summit supercomputers, where each platform requires different caching and fusion strategies
		to perform optimally. We show that SDFGs deliver competitive performance, allowing domain scientists to develop
		applications naturally and port them to approach peak hardware performance without modifying the original scientific
		code.

		<h4><b>Bio:</b>
		Torsten is an Associate Professor of Computer Science at ETH Zürich,
		Switzerland. Before joining ETH, he led the performance modeling and
		simulation efforts of parallel petascale applications for the NSF-funded
		Blue Waters project at NCSA/UIUC.  He is also a key member of the
		Message Passing Interface (MPI) Forum where he chairs the "Collective
		Operations and Topologies" working group.  Torsten won best paper awards
		at the ACM/IEEE Supercomputing Conference SC10, SC13, SC14, EuroMPI'13,
		HPDC'15, HPDC'16, IPDPS'15, and other conferences.  He published
		numerous peer-reviewed scientific conference and journal articles and
		authored chapters of the MPI-2.2 and MPI-3.0 standards. He received the
		Latsis prize of ETH Zurich as well as an ERC starting grant in 2015. His
		research interests revolve around the central topic of
		"Performance-centric System Design" and include scalable networks,
		parallel programming techniques, and performance modeling. Additional
		information about Torsten can be found on his homepage at
		htor.inf.ethz.ch.	
	  </div>

      <div class="midBox1">
        <h2>Coffee break 10:00 - 10:30 am</h2>
      </div>

      <div class="midBox1">
        <h1>Session 1: GPU Algorithms</h1>
        <h3>10:30 am - 12:00 pm<br />Session Chair: Stefano Markidis, KTH Royal Institute of Technology, Sweden</h3>
        <ul>
          <li>
            <b>Javelin: A Scalable Implementation for Sparse Incomplete LU Factorization</b><br />
            Joshua Booth, and Gregory Bolet
          </li>
          <li>
            <b>Approximate and Exact Selection on GPUs [Invited]</b><br />
            Tobias Ribizel, and Hartwig Anzt
          </li>
          <li>
            <b>An Adaptive Algorithm for Parallel Sparse Triangular Solve on Heterogeneous Processors [Invited]</b><br />
			Weifeng Liu, and Hemeng Wang<br/>
            [<a href="abstracts/Weifeng_Liu_AsHES19_invited_talk_abstract.txt" target="_blank">Abstract</a>]
          </li>
        </ul>
      </div>

      <div class="midBox1">
        <h2>Lunch break 12:00 - 1:00 pm</h2>
      </div>

      <div class="midBox1">
        <h1>Session 2: Communication and Memory</h1>
        <h3>1:00 - 2:30 pm<br />Session Chair: Hartwig Anzt, Karlsruhe Institute of Technology, Germany & University of Tennessee, USA</h3>
        <ul>
          <li>
            <b>Parallel Processing on FPGA Combining Computation and Communication in OpenCL Programming</b><br />
            Norihisa Fujita, Ryohei Kobayashi, Yoshiki Yamaguchi, and Taisuke Boku
          </li>
          <li>
            <b>GPU-FPGA Heterogeneous Computing with OpenCL-enabled Direct Memory Access</b><br />
            Ryohei Kobayashi, Norihisa Fujita, Yoshiki Yamaguchi, Ayumi Nakamichi, and Taisuke Boku
          </li>
          <li>
            <b>Evaluating the Impact of High-Bandwidth Memory on MPI Communications [Invited]</b><br />
			Giuseppe Congiu, and Pavan Balaji<br />
			[<a href="abstracts/Giuseppe_Congiu_AsHES19_invited_talk_abstract.pdf" target="_blank">Abstract</a>]
			[<a href="presentations/AsHES - HeteroMem.pptx" target="_blank">Presentation</a>]
          </li>
        </ul>
      </div>

      <div class="midBox1">
        <h2>Coffee break 2:30 - 3:00 pm</h2>
      </div>

      <div class="midBox1">
        <h1>Session 3: Performance and Energy Analysis</h1>
        <h3>3:00 - 4:00 pm<br />Session Chair: Giuseppe Congiu, Argonne National Laboratory, USA</h3>
        <ul>
          <li>
            <b>Analysis of Energy Efficiency of a Parallel AES Algorithm for CPU-GPU Heterogeneous Platforms</b><br />
			Xiongwei Fei, Kenli Li, Wangdong Yang, and Keqin Li
          </li>
          <li>
            <b>TensorFlow Doing HPC [Invited]</b><br />
           	Steven Wei-Der Chien, Stefano Markidis, Vyacheslav Olshevsky, Yaroslav Bulatov, Erwin Laure, and Jeffrey Vetter
          </li>
        </ul>
      </div>

<!--
	  <div class="midBox1">
      	<h1>Closing Remarks</h1>
      	<h3>4:30 pm</h3>
      	</div>
	  </div>
-->
    	<?php include('footer.php'); ?>
    </div>
  </body>
</html>
