<h1>Journal Special Issue</h1>
<div id="description">
  <p>The best papers of AsHES 2019 will be invited to a <b>Special Issue on Topics on Heterogeneous Computing</b> of the <a href="http://www.journals.elsevier.com/parallel-computing/">Elsevier International Journal on Parallel Computing (PARCO)</a> (<a href = si_archive.php>Link to Speical Issues List</a>).

<!-- <p><a href = "https://www.sciencedirect.com/science/article/pii/S0167819118301844">ASHES 2017 SI</a>:
Special issue on applications for the heterogeneous computing era 2017.
Edited by Sunita Chandrasekaran and Antonio J. Peña.
</p>

<p><a href = "https://www.sciencedirect.com/science/article/pii/S0167819117301084">ASHES 2016 SI</a>:
Special Issue on Topics on Heterogeneous Computing.
Edited by Sunita Chandrasekaran and Antonio J. Peña
</p>

<p><a href = "https://journals.sagepub.com/doi/full/10.1177/1094342014527657">ASHES 2013 SI</a>:
Special Issue on Applications for the Heterogeneous Computing Era.
Edited by Jiayuan Meng and Toshio Endo.
</p>

<p><a href = "https://journals.sagepub.com/doi/full/10.1177/1094342012442457">CACHES 2011 SI</a>:
Applications for the Heterogeneous Computing Era.
Edited by Pavan Balaji and Jiayuan Meng (2012).
</p> -->

</div>
