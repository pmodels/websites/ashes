<html>
  <head>
    <title>AsHES Workshop</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">

      <?php include('header.php'); ?>
      <div id="sub-frame">

        <h1>Steering Committee</h1>
        <div id="description">
          <p><b><a href="http://www.mcs.anl.gov/~balaji">Pavan Balaji</a></b>, Argonne National Laboratory, USA</p>
          <p><b>Yunquan Zhang</a></b>, Chinese Academy of Sciences, China</p>
          <p><b><a href="http://matsu-www.is.titech.ac.jp/~matsu/">Satoshi Matsuoka</a></b>, Tokyo Institute of Technology, Japan</p>
          <p><b><a href="https://scholar.google.com/citations?user=XJi8zQ8AAAAJ&hl=en">Jiayuan Meng</a></b>, Argonne National Laboratory, USA</p>
          <p><b><a href="http://qcri.org.qa/page?a=117&name=Xiaosong_Ma&pid=154&lang=en-CA">Xiaosong Ma</a></b>, Qatar Computing Research Institute, Qatar</p>
          <p><b><a href="https://www.iacs.stonybrook.edu/people/faculty/barbara-chapman">Barbara Chapman</a></b>, Stony Brook University, USA</p>
          <p><b><a href="http://www.capsl.udel.edu/~ggao/">Guang R. Gao</a></b>, University of Delaware, USA</p>
          <p><b>Xinmin Tian</b>, Intel, USA</p>
          <p><b>Michael Wong</b>, Codeplay, UK</p>
          <p><b><a href="http://scholar.google.com/citations?user=imdGgDAAAAAJ&hl=en">James Dinan</a></b>, Intel Corporation</p>
          <p><b><a href= "https://www.eecis.udel.edu/~schandra/">Sunita Chandrasekaran</a></b>, University of Delaware, USA</p>
        </div>

        <h1>General Chair</h1>
        <div id="description">
          <p><b><a href = "http://www.bsc.es/about-bsc/staff-directory/pena-antonio">Antonio J. Pe&ntilde;a</a></b>, Barcelona Supercomputing Center, Spain</p>
        </div>

        <h1>Program Co-Chairs</h1>
        <div id="description">
          <p><b><a href = "http://www.mcs.anl.gov/~minsi/">Min Si</a></b>, Argonne National Laboratory, USA</p>
		  <p><b><a href = "https://www.lenaoden.de/">Lena Oden</a></b>, FernUni Hagen, Germany</p>
        </div>

        <h1>Web Chair</h1>
        <div id="description">
            <p><b><a href = "https://www.rohitzambre.com">Rohit Zambre</a></b>, University of California - Irvine, USA</p>
        </div>

        <h1>Technical Program Committee</h1>
		<div id="description">
            <p><b><a href = "https://scholar.google.com/citations?user=heDaFFYAAAAJ&hl=en">Ashwin Aji</a></b>, AMD, USA</p>
            <p><b>James Beyer</b>, NVIDIA Corporation, USA</p>
            <p><b><a href = "http://www.mcs.anl.gov/~sriduttb ">Sridutt Bhalachandra</a></b>, Argonne National Laboratory, USA</p>
            <p><b><a href = "https://sites.google.com/a/uji.es/adrian-castello/home">Adrián Castelló</a></b>, Universitat Jaume I, Spain</p>
			<p><b><a href = "http://www.carch.ac.cn/~huimin/main.html">Huimin Cui</a></b>, Institute of Computing Technology, CAS, China</p>
            <p><b>Khaled Hamidouche</b>, AMD, USA<p>
            <!--<p><b><a href = "https://scholar.google.com/citations?user=KRYhZggAAAAJ&hl=en">Khaled Hamidouche</a></b>, AMD, USA<p>-->
            <p><b><a href = "https://jeffhammond.github.io/">Jeff Hammond</a></b>, Intel Corporation, USA</p>
            <p><b><a href = "https://www.fz-juelich.de/SharedDocs/Personen/IAS/JSC/EN/staff/herten_a.html">Andreas Herten</a></b>, Juelich Supercomputing Center, Forschungszentrum Juelich, Germany</p>
            <p><b><a href = "http://people.cs.vt.edu/~kaixihou/">Kaixi Hou</a></b>, NVIDIA Corporation, USA</p>
			<p><b>Gabriele Jost</b>, NASA Ames Research Center/Supersmith, USA</p>
            <p><b><a href= "https://www.hzdr.de/db/!ContMan.Visi.Card?pNid=no&pUser=4930">Guido Juckeland</a></b>, Helmholtz-Zentrum Dresden-Rossendorf (HZDR), Germany </p>
            <p><b><a href = "http://ft.ornl.gov/~lees2/">Seyong Lee</a></b>, Oak Ridge National Laboratory, USA</p>
            <p><b>John Leidel</b>, Texas Tech University, USA</p>
            <p><b><a href = "http://www.icl.utk.edu/~luszczek/">Piotr Luszczek</a></b>, University of Tennessee, USA</p>
            <p><b><a href = "https://scholar.google.com/citations?user=jn3QXz4AAAAJ&hl=en">Naoya Maruyama</a></b>, Lawrence Livermore National Laboratory, USA</p>
            <p><b><a href = "https://cfwebprod.sandia.gov/cfdocs/CompResearch/templates/insert/profile.cfm?slolivi">Stephen Olivier</a></b>, Sandia National Laboratories, USA</p>
            <p><b><a href = "https://grypp.github.io/">Guray Ozen</a></b>, NVIDIA Corporation/PGI, Germany</p>
            <p><b>Barry L. Rountree</b>, Lawrence Livermore National Laboratory, USA</p>
            <p><b><a href = "https://people.llnl.gov/desupinski1">Bronis R. de Supinski</a></b>, Lawrence Livermore National Laboratory, USA</p>
            <p><b><a href = "https://scholar.google.com/citations?user=KUaGCvkAAAAJ&hl=en">Xiaonan Tian</a></b>, NVIDIA Corporation, USA</p>
            <p><b><a href = "https://www.bsc.es/valero-lara-pedro">Pedro Valero-Lara</a></b>, Barcelona Supercomputing Center, Spain</p>
        </div>

<?php /*
        <h1>Journal Special Issue Co-editors</h1>
        <div id="description">
          TBD.
        </div>
*/ ?>
      </div>

      <?php include('footer.php'); ?>

    </div>
  </body>
</html>
