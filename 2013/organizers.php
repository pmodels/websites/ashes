<html>
  <head>
    <title>AsHES Workshop</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">

      <?php include('header.php'); ?>
      <div id="sub-frame">

		<h1>Steering Committee</h1>
        <div id="description">
          <p><b><a href="http://www.cs.virginia.edu/~jm6dg/">Jiayuan
          Meng</a></b>, Argonne National Laboratory (co-chair)</p>
          <p><b><a href="http://www.mcs.anl.gov/~balaji">Pavan
          Balaji</a></b>, Argonne National Laboratory (co-chair)</p>
          <p><b><a href="http://matsu-www.is.titech.ac.jp/~matsu/">Satoshi
          Matsuoka</a></b>, Tokyo Institute of Technology</p>
        </div>

		<h1>General Chair</h1>
        <div id="description">
          <p><b><a href="http://www.cs.virginia.edu/~jm6dg/">Jiayuan
          Meng</a></b>, Argonne National Laboratory</p>
        </div>
          	
		<h1>Program Chairs</h1>
        <div id="description">
          <p><b><a href="http://www.rdcps.ac.cn/~zyq/zyq-en.html">Yunquan
          Zhang</a></b>, Chinese Academy of Sciences</p>
          <p><b><a href="http://www4.ncsu.edu/~xma/">Xiaosong
          Ma</a></b>, North Carolina State University</p>
        </div>

		<h1>Publicity Chairs</h1>
        <div id="description">
		<p><b><a href="http://www.cs.wayne.edu/~weisong/">Weisong Shi</a></b>, Wayne State University</p>
        </div>

        <h1>Web Chair</h1>
        <div id="description">
          <p><b><a href="http://www.rdcps.ac.cn/">Liang
          Yuan</a></b>, Chinese Academy of Sciences</p>
		  <p><b>Feng Ji</b>, North Carolina State University</p>
        </div>
          	
		<h1>Technical Program Committee</h1>
        <div id="description">
		<p><b>Hong An</b>, University of Science and Technology of China, China</p>
		<p><b>David Bader</b>,     Georgia Institute of Technology, USA</p>
		<p><b>Surendra Byna</b>,     Lawrence Berkeley National Laboratory, USA</p>
		<p><b>Ali R. Butt</b>,     Virginia Tech, USA</p>
		<p><b>Wenguang Chen</b>,   Tsinghua University, China</p>
		<p><b>Xiaobing Feng</b>,   Institute of Computing Technology, CAS, China</p>
		<p><b>Torsten Hoefler</b>, ETH Zürich., Switzerland</p>
		<p><b>Heshan Lin</b>,     Virginia Tech, USA</p>
		<p><b>Yutong Lu</b>,       National University of Defense Technologies, China</p>
		<p><b>Naoya Maruyama</b>,  Tokyo Institute of Technology, Japan</p>
		<p><b>Frank Mueller</b>, North Carolina State University, USA</p>
		<p><b>Dimitris Nikolopoulos</b>,   University of Crete/FORTH, Greece</p>
		<p><b>P. Sadayappan</b>, Ohio State University, USA</p>
		<p><b>Xipeng Shen</b>, College of William and Mary, USA</p>
		<p><b>Bronis de Supinski</b>, Lawrence Livermore National Laboratory, USA</p>
		<p><b>Vinod Tipparaju</b>, Advanced Micro Devices, Inc., USA</p>
		<p><b>Dawei Wang</b>, Illinois Institute of Technology, USA</p>
		<p><b>Fangfang Xia</b>, Argonne National Laboratory, USA</p>



        </div>
          	
		<h1>Journal Special Issue Co-editors</h1>
        <div id="description">
          <p><b><a href="http://www.cs.virginia.edu/~jm6dg/">Jiayuan
          Meng</a></b>, Argonne National Laboratory</p>
          <p><b><a href="http://matsu-www.is.titech.ac.jp/~endo/">Toshio ENDO</a></b>, Tokyo Institute of
          Technology</p>
          <br><br>
        </div>
          	
      </div>

      <?php include('footer.php'); ?>

    </div>
  </body>
</html>
