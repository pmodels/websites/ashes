<html>
  <head>
    <title>AsHES Workshop</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">

      <?php include('header.php'); ?>
      <div id="sub-frame">

        <br>

		<h1>Workshop Scope and Goals</h1>

        <div id="description">
          <img src="pics/boston-skyline.jpg" border="3" width="320" align="left"/>

          <p>As we look beyond the petascale era, accelerators such 
             as Graphics Processing Units (GPUs) and FPGAs, as well 
             as upcoming integrated hybrid processing cores, are 
             expected to play a preeminent role in architecting the 
             largest systems
          in the world. While there is significant interest in these
          architectures, much of this interest is an artifact of the
          hype associated with them. This workshop focuses on
          understanding the implications of accelerators 
          on the architectures and programming environments of future
          systems.
          It seeks to ground accelerator research
          through studies of application kernels or whole applications
          on such systems, as well as tools and libraries that improve
          the performance or productivity of applications trying to
          use these systems.</p>

          <p>The goal of this workshop is to bring together
          researchers and practitioners who are involved in
          application studies for accelerators and other hybrid
          systems, to learn the opportunities and challenges in future
          design trends for HPC applications and systems.</p>

	      <br>
        </div>

        <?php include('journal.php'); ?>
	    <br>
        
        <?php include('dates.php'); ?>
		
	    <h1>Previous Workshops</h1>
        <div id="description">
          <p><a href="http://www.mcs.anl.gov/events/workshops/ashes/2012">ASHES
              2012</a> in Shanghai, China</p>
          <p><a href="http://www.mcs.anl.gov/events/workshops/ashes/2011">CACHES
              2011</a> in Tucson, Arizona</p>
        </div>

      </div>
      <?php include('footer.php'); ?>

    </div>
  </body>
</html>
