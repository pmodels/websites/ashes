<html>
  <head>
    <title>AsHES Workshop</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">

      <?php include('header.php'); ?>
	</ul>

<div class="midBox1">
<h1>Opening remarks</h1>
<h3>8:30 - 8:45</h3>
<img src="images/open_remarks_Yunquan1.jpg" height="180" alt="" />
<img src="images/open_remarks_Toshio1.jpg" height="180" alt="" />
<img src="images/Close remarks Dong.jpg" height="180" alt="" />
<img src="images/Session2 Chair Yong.jpg" height="180" alt="" />
<img src="images/Session3 Chair David.jpg" height="180" alt="" />
</div>

<div class="midBox1">
<h1>Keynote</h1>
<h3>8:45 - 9:45</h3>
<h4><a href="keynote.php"><b>What first principles of algorithms and architectures says about heterogeneity</b></a></h4>
<h4>Prof. Richard Vuduc, Georgia Institute of Technology <a href="http://hpcgarage.org/ashes2013/">Slides</a> </h4>
<img src="images/overview_keynote.jpg" height="180" alt="" />
<img src="images/keynote_Rich.jpg" height="180" alt="" />
<h4><i>Abstract:</i>
<p>In this talk, we will "pop up a level" and ask whether first
principles of algorithm and architecture design can tell us anything
about heterogeneity. In particular, I'll posit "strawman" cost models
that connect properties of an algorithm with physical properties of
machines, like energy, power, and area. I will then explain what these
models imply. For instance, I'll use them to evaluate the time- and
energy-efficiency of heterogeneous designs as well as for predicting
the benefits of other forms of heterogeneity, such as "communication"
heterogeneity. And although I'll show a bunch of formulas, calculate
things, and plot them, I stress that this talk is about ideas, rather
than a well-developed set of results. As such, your questions, healthy
skepticism, (constructive!) feedback, and offers of collaboration may
be even more welcome than usual! :-) (<a href="keynote.php">Read...</a>)</p>
</h4>
</div>

<div class="midBox1">
<h2>Break 9:45 - 10:15</h2>
</div>

<div class="midBox1">
<h1>Session 1: Programing Model and Performance Optimizations</h1>
<h3>10:15 - 11:55</h3>
<h4>Session Chair: Toshio ENDO, Tokyo Institute of Technology </h4>


        <div>
		<img src="images/Talk1.jpg" height="180" alt="" />
		<img src="images/Talk2.jpg" height="180" alt="" />
		<img src="images/Talk3.jpg" height="180" alt="" />
		<img src="images/Talk4.jpg" height="180" alt="" />
        </div>

<ul>
	<li>
	<b>Synchronization and Ordering Semantics in Hybrid MPI+GPU Programming</b> </br>
        Ashwin Aji, Pavan Balaji, James Dinan, Wuchun Feng and Rajeev Thakur.
	<a href="slides/Aji_ashes13.pdf">Slides</a> 
        </li>
	<li>
	<b>Tightly Coupled Accelerators Architecture for Minimizing Communication Latency among Accelerators</b> </br>
	Toshihiro Hanawa, Yuetsu Kodama, Taisuke Boku and Mitsuhisa Sato.
	<a href="slides/Hanawa_ashes13.pdf">Slides</a> 
        </li>
	<li>
	<b>Analyzing Optimization Techniques for Power Efficiency on Heterogeneous Platforms</b> </br>
	Yash Ukidave and David Kaeli.
	<a href="slides/Ukidave_ashes13.pdf">Slides</a>
        </li>
	<li>
	<b>Composing multiple Starpu applications over heterogeneous machines: a supervised approach</b> </br>
	Andra-Ecaterina Hugo, Abdou Guermouche, Pierre-Andre Wacrenier and Raymond Namyst.
	<a href="slides/Hugo_ashes13.pdf">Slides</a>
        </li>
        <br>
	</ul>
</div>

<div class="midBox1">
<h2>Lunch 11:55 am - 1:30 pm</h2>
</div>

<div class="midBox1">
<h1>Session 2: Accelerated Applications</h1>
<h3>1:30 - 3:10 pm</h3>
<h4>Session Chair: Yong Chen, Texas Tech University </h4>i

        <div>
		<img src="images/Talk5.jpg" height="180" alt="" />
		<img src="images/Talk6.jpg" height="180" alt="" />
		<img src="images/Talk7.jpg" height="180" alt="" />
		<img src="images/Talk8.jpg" height="180" alt="" />
        </div>


<ul>
	<li>
	<b> Fast, Scalable Parallel Comparison Sort on Hybrid Multicore Architectures</b> </br>
        Dip Sankar Banerjee, Parikshit Sakurikar and Kishore Kothapalli.
	<a href="slides/Banerjee_ashes13.pdf">Slides</a>
        </li>
	<li>
	<b> Tridiagonalization of a symmetric dense matrix on a GPU cluster</b> </br>
	Ichitaro Yamazaki.
	<a href="slides/Yamazaki_ashes13.pdf">Slides</a>
        </li>
	<li>
	<b>A Multi-level Optimization Method for Stencil Computation on the  Domain that is Bigger than Memory Capacity of GPU</b> </br>
	Guanghao Jin, Toshio Endo and Satoshi Matsuoka.
	<a href="slides/Jin_ashes13.pdf">Slides</a>
        </li>
	<li>
	<b>Accelerating the 3D Elastic Wave Forward Model on GPU and MIC</b> </br>
	Yang You, Haohuan Fu, Guangwen Yang and Xiaomeng Huang.
	<a href="slides/Yang_ashes13.pdf">Slides</a> 
        </li>
        <br>
	</ul>
</div>

<div class="midBox1">
<h2>Break 3:10 - 3:40 pm</h2>
</div>

<div class="midBox1">
<h1>Session 3: Emerging Hybrid Systems</h1>
<h3>3:40 - 5:20 pm</h3>
<h4>Session Chair: David Kaeli, Northeastern University </h4>


        <div>
		<img src="images/Talk9.jpg" height="180" alt="" />
		<img src="images/Talk10.jpg" height="180" alt="" />
		<img src="images/Talk12.jpg" height="180" alt="" />
        </div>


<ul>
	<li>
	<b>Improving GPU Performance Prediction with Data Transfer Modeling</b> </br>
        Michael Boyer, Jiayuan Meng and Kalyan Kumaran. 
	<a href="slides/Boyer_ashes13.pdf">Slides</a>
        </li>
	<li>
	<b>Use of SIMD Vector Operations to Accelerate Application Code Performance on Low-Powered ARM and Intel Platforms</b> </br>
	Gaurav Mitra, Beau Johnston, Alistair P. Rendell, Eric McCreath and Jun Zhou.
	<a href="slides/Gaurav_ashes13.pdf">Slides</a>
        </li>
	<li>
	<b>Using MIC to accelerate a typical data-intensive application: the Breadth-first Search</b> </br>
	Gao Tao, Lu Yutong and Suo Guang.
	<a href="slides/Tao_ashes13.pdf">Slides</a>
        </li>
	<li>
	<b>Dynamic Load Balancing of the Adaptive Fast Multipole Method in Heterogeneous Systems</b> </br>
	Robert Overman, Jan Prins, Laura Miller and Michael Minion.
	<a href="slides/Overman_ashes13.pdf">Slides</a>
        </li>
        <br>
	</ul>
</div>


    </div>

<?php include('footer.php'); ?>
</body>
</html>
