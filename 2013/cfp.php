<html>
  <head>
    <title>AsHES Workshop</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">

      <?php include('header.php'); ?>
      <div id="sub-frame">

		<h1>Scope</h1>
        <div id="description">

          <p> The goal of this workshop is to bring together researchers and practitioners who are involved in application studies for accelerators and other hybrid systems, to learn the opportunities and challenges in future design trends for HPC applications and systems.</p>

          <p>The CFP can also be found in 
          <a href="http://people.engr.ncsu.edu/fji/doc/ASHES2013_CFP.txt">txt</a>, 
          <a href="http://people.engr.ncsu.edu/fji/doc/ASHES2013_CFP.doc">Word</a> 
		  and <a href="http://people.engr.ncsu.edu/fji/doc/ASHES2013_CFP.pdf">PDF</a>. 
        </div>
				
        <div id="description">
          <p>We are soliciting contributions in areas including but
          not limited to:</p>

          <ul>
            <li>Characterizing strategies for implementing and
            optimizing HPC applications for accelerators.</li>
            <li>Techniques for optimizing kernels for execution on
            GPUs and future hybrid platforms.</li>
            <li>Modeling of applications running on accelerators and hybrid HPC
            systems.</li>
            <li>Implication of workload characterization in
            hybrid design issues.</li>
            <li>Benchmarking and performance evaluation for
            accelerator units.</li>
            <li>Tools and techniques to assist application development
            for accelerators and hybrid processors.</li>
            <li>System software techniques to abstract application
            domain-specific functionalities for accelerators.</li>
          </ul>
        </div>
				
		<h1>Proceedings</h1>
        <div id="description">
          <p>The proceedings of this workshop will be published
          electronically together with with IPDPS proceedings via the
          IEEE Xplore Digital Library.</p>
        </div>

        <?php include('instructions.php'); ?>
        <?php include('journal.php'); ?>
        <?php include('dates.php'); ?>
				
      </div>

      <?php include('footer.php'); ?>

    </div>
  </body>
</html>
