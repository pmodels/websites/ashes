<h1>Important Dates</h1>
<div id="description">
  <p><b>Paper Submission:</b> <s>Jan. 11, 2013</s> Jan. 22, 2013</p>
  <p><b>Rebuttal Deadline:</b> Feb. 18, 2013 AOE</p>
  <p><b>Author Notification:</b> Feb. 28, 2013</p>
  <p><b>Camera-Ready Deadline:</b> Mar. 8, 2013</p>
  <p><b>Workshop:</b> May 20, 2013</p>
</div>

