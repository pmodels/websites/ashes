<h1>Journal Special Issue</h1>
<div id="description">

  <p>The best papers of AsHES 2013 will be included in a <bold>
  Special Issue on Applications for the Heterogeneous Computing
  Era</bold> of the <a href="http://hpc.sagepub.com/">International
  Journal of High Performance Computing Applications</a>, edited by
  Jiayuan Meng and Toshio ENDO (<a href = si_archive.php>Link to past Speical Issues</a>).</p>

</div>
