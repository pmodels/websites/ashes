===========================================================
          AsHES 2015  ++  CALL FOR PAPERS (FINAL)   
===========================================================

The 5th Intl. Workshop on Accelerators & Hybrid Exascale Systems
May 25th, 2015,  Hyderabad, India
In conjunction with IPDPS 2015
http://www.mcs.anl.gov/events/workshops/ashes/2015

Important Dates:
========================================

Paper Submission: Jan. 28, 2015 (final, including short format)
Author Notification: Feb. 14, 2015
Camera-Ready Deadline: Feb. 28, 2015

Workshop Scope and Goals:
========================================

As we look beyond the petascale era, accelerator and heterogeneous
architectures are expected to play a preeminent role in architecting the
largest systems in the world. Future systems may not only offer
accelerators (e.g. GPGPU, Intel(R) Xeon Phi(tm), FPGA) and hybrid
processors of both lightweight and heavyweight cores (e.g APU,
big.LITTLE), but also may use hybrid memory systems equipped with
stacked memory and non-volatile memory in addition to regular DRAM.
While there is significant interest in these architectures, much of it
is an artifact of the hype associated with them. This workshop focuses
on understanding the implications of accelerators and heterogeneous
designs on the hardware systems, applications, and programming
environments of future systems. It seeks to ground accelerator research
through studies of application kernels or whole applications on such
systems, as well as tools and libraries that improve the performance and
productivity of applications on these systems.

The goal of this workshop is to bring together researchers and
practitioners who are involved in application studies for accelerators
and heterogeneous systems, to learn the opportunities and challenges in
future design trends for HPC applications and systems.  We are
soliciting contributions in areas including but not limited to:

 * Strategies for application behavior characterization and performance
   optimization for accelerators;
 * Techniques for optimizing kernels for execution on GPGPU, Intel(R)
   Xeon Phi(tm), and future heterogeneous platforms;
 * Models of application performance on heterogeneous and accelerated
   HPC systems;
 * Implications of workload characterization in heterogeneous and
   accelerated architecture design;
 * Benchmarking and performance evaluation for accelerators;
 * Tools and techniques to assist application development for
   accelerators and heterogeneous processors;
 * System software techniques to abstract application domain-specific
   functionalities for accelerators.

Proceedings and Submission Instructions:
========================================

The proceedings of this workshop will be published electronically
together with with IPDPS proceedings via the IEEE Xplore Digital
Library.

Submitted manuscripts may not exceed 10 single-spaced double-column pages
using 10-point size font on 8.5x11 inch pages (IEEE conference style),
including figures, tables, and references. See the IPDPS website for
details and templates.

Short format papers at least 5 pages in length will also be accepted.

Submission site: https://www.easychair.org/conferences/?conf=ashes15

Journal Special Issue:
========================================

The best papers of AsHES 2015 will be included in a Special Issue on
Applications for the Heterogeneous Computing Era of the International
Journal of High Performance Computing Applications.

Organizers:
========================================

General Chair:

Xiaosong Ma, Qatar Computing Research Institute

Technical Program Committee:

James Dinan (chair), Intel Corporation
Wenguang Chen (chair), Tsinghua University
Jeff Hammond (publicity), Intel Corporation
Rong Ge, Marquette University
Hao Wang, Virginia Tech
Wenguang Chen, Tsinghua University
Fangfang Xia, Argonne National Laboratory
Yongpeng Zhang, Stone Ridge Technology
Antonio Pe�a, Argonne National Laboratory
Naoya Maruyama, RIKEN AICS
Suren Byna, Lawrence Berkeley National Laboratory
Bronis de Supinski, Lawrence Livermore National Laboratory
Sriram Krishnamoorthy, Pacific Northwest National Laboratory
Huimin Cui, Institute of Computing Technology, CAS
Guangyu Sun, Peking University
Siva Kumar Sastry Hari, NVIDIA Corporation
Xipeng Shen, North Carolina State University
Kelly Shaw, University of Richmond
Khaled Hamidouche, The Ohio State University
Gabriele Jost, Intel Corporation
Feng Ji, Google
Junli Gu, AMD
