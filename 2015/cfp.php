<html>
  <head>
    <title>AsHES Workshop</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">

      <?php include('header.php'); ?>
      <div id="sub-frame">

		<h1>Scope</h1>
        <div id="description">
<p> As we look beyond the petascale era, accelerator and heterogeneous architectures are expected to play a preeminent role in architecting the largest systems in the world. Future systems may not only offer accelerators (e.g. GPGPU, Intel&reg; Xeon Phi&trade;, FPGA) and hybrid processors of both lightweight and heavyweight cores (e.g APU, big.LITTLE), but also may use hybrid memory systems equipped with stacked memory and non-volatile memory in addition to regular DRAM. While there is significant interest in these architectures, much of it is an artifact of the hype associated with them. This workshop focuses on understanding the implications of accelerators and heterogeneous designs on the hardware systems, applications, and programming environments of future systems. It seeks to ground accelerator research through studies of application kernels or whole applications on such systems, as well as tools and libraries that improve the performance and productivity of applications on these systems.</p>

          <p>The goal of this workshop is to bring together researchers and practitioners who are involved in application studies for accelerators and heterogeneous systems, to learn the opportunities and challenges in future design trends for HPC applications and systems.</p>

        </div>
				
        <div id="description">
          <p>We are soliciting contributions in areas including but
          not limited to:</p>

          <ul>
            <li>Strategies for application behavior characterization and performance optimization for accelerators;</li>
            <li>Techniques for optimizing kernels for execution on GPGPU, Intel&reg; Xeon Phi&trade;, and future heterogeneous platforms;</li>
            <li>Models of application performance on heterogeneous and accelerated HPC systems;</li>
            <li>Implications of workload characterization in heterogeneous and accelerated architecture design;</li>
            <li>Benchmarking and performance evaluation for accelerators;</li>
            <li>Tools and techniques to assist application development for accelerators and heterogeneous processors;</li>
            <li>System software techniques to abstract application domain-specific functionalities for accelerators.
</li>
          </ul>
        </div>
				
		<h1>Proceedings</h1>
        <div id="description">
          <p>The proceedings of this workshop will be published
          electronically together with with IPDPS proceedings via the
          IEEE Xplore Digital Library.</p>

        </div>

        <?php include('instructions.php'); ?>
        <?php include('journal.php'); ?>
        <?php include('dates.php'); ?>
				
      </div>

      <?php include('footer.php'); ?>

    </div>
  </body>
</html>
