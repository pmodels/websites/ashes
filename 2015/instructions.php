<h1>Submission Instructions</h1>
<div id="description">
  <p>Submitted manuscripts may not exceed 10 single-spaced
  double-column pages using 10-point size font on 8.5x11 inch pages
  (IEEE conference style), including figures, tables, and
  references. See the style templates
  for <a href="http://www.ipdps.org/templates/IEEECS_CPS_LaTeX_Letter_2Col.zip">latex</a>
  or <a href="http://www.ipdps.org/templates/IEEECS_CPS_8.5x11x2.zip">word</a>
  for details.</p>

  <p>Short format papers at least 5 pages in length will also be accepted.</p>

  <p>Submissions will be judged based on relevance, significance,
  originality, correctness and clarity.</p>

  <p>Submissions can be made at <a
  href="https://www.easychair.org/conferences/?conf=ashes15">this
  link</a>.</p>
</div>
