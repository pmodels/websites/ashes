<html>
  <head>
    <title>AsHES Workshop</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">

      <?php include('header.php'); ?>
      <div id="sub-frame">

        <h1>Steering Committee</h1>
        <div id="description">
          <p><b><a href="http://www.mcs.anl.gov/~balaji">Pavan Balaji</a></b>, Argonne National Laboratory (co-chair)</p>
          <p><b><a href="http://www.rdcps.ac.cn/~zyq/zyq-en.html">Yunquan Zhang</a></b>, Chinese Academy of Sciences</p>
          <p><b><a href="http://matsu-www.is.titech.ac.jp/~matsu/">Satoshi Matsuoka</a></b>, Tokyo Institute of Technology</p>
          <p><b><a href="http://www.cs.virginia.edu/~jm6dg/">Jiayuan Meng</a></b>, Argonne National Laboratory (co-chair)</p>
          <p><b><a href="http://qcri.org.qa/page?a=117&name=Xiaosong_Ma&pid=154&lang=en-CA">Xiaosong Ma</a></b>, Qatar Computing Research Institute</p>
        </div>

        <h1>General Chair</h1>
        <div id="description">
          <p><b><a href="http://qcri.org.qa/page?a=117&name=Xiaosong_Ma&pid=154&lang=en-CA">Xiaosong Ma</a></b>, Qatar Computing Research Institute</p>
        </div>

        <h1>Program Chairs</h1>
        <div id="description">
          <p><b><a href="http://scholar.google.com/citations?user=imdGgDAAAAAJ&hl=en">James Dinan</a></b>, Intel Corporation</p>
          <p><b><a href="http://hpc.cs.tsinghua.edu.cn/research/cluster/cwg.html">Wenguang Chen</a></b>, Tsinghua University</p>
        </div>

        <h1>Publicity Chairs</h1>
        <div id="description">
          <p><b>Jeff Hammond</b>, Intel Corporation</p>
        </div>

<!--
        <h1>Web Chair</h1>
        <div id="description">
          <p><b></b>TBD</p>
        </div>
-->

        <h1>Technical Program Committee</h1>
        <div id="description">
            <p><b>Rong Ge</b>, Marquette University</p>
            <p><b>Hao Wang</b>, Virginia Tech</p>
            <p><b>Fangfang Xia</b>, Argonne National Laboratory</p>
            <p><b>Yongpeng Zhang</b>, Stone Ridge Technology</p>
            <p><b>Antonio Pe&ntilde;a</b>, Argonne National Laboratory</p>
            <p><b>Naoya Maruyama</b>, RIKEN AICS</p>
            <p><b>Suren Byna</b>, Lawrence Berkeley National Laboratory</p>
            <p><b>Bronis de Supinski</b>, Lawrence Livermore National Laboratory</p>
            <p><b>Sriram Krishnamoorthy</b>, Pacific Northwest National Laboratory</p>
            <p><b>Huimin Cui</b>, Institute of Computing Technology, CAS</p>
            <p><b>Guangyu Sun</b>, Peking University</p>
            <p><b>Siva Kumar Sastry Hari</b>, NVIDIA Corporation</p>
            <p><b>Xipeng Shen</b>, North Carolina State University</p>
            <p><b>Kelly Shaw</b>, University of Richmond</p>
            <p><b>Khaled Hamidouche</b>, The Ohio State University</p>
            <p><b>Gabriele Jost</b>, Intel Corporation</p>
            <p><b>Feng Ji</b>, Google</p>
            <p><b>Junli Gu</b>, AMD</p>
        </div>

        <h1>Journal Special Issue Co-editors</h1>
        <div id="description">
            <p><b></b>TBD</p>
            <br><br>
        </div>

      </div>

      <?php include('footer.php'); ?>

    </div>
  </body>
</html>
