<html>
  <head>
    <title>AsHES Workshop: AsHES 2020 Preliminary Program</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">
      <?php include('header.php'); ?>

	  <div id="sub-frame">
    
    <div class="midBox1">
        <h1> AsHES 2020 Virtual Presentation </h1>
        <iframe width="924" height="520" src="https://www.youtube.com/embed/9ss9M4Ei6Cc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <!-- <h1>Important Note</h1>
        <h3><font color="#CC0000">Workshop connection information will be announced to registered attendees by email (<a href=https://forms.gle/EBmxt2mzt6H7SWT59>registration link</a>).</font></h3> -->

        <h1>Keynote (5:30 pm CDT)</h1>
        <h4><b>Multi-Hetero Accelerated Supercomputing: System, Programming and Applications</b></h4>
        <h3>Taisuke Boku, Center for Computational Sciences, University of Tsukuba</h3>
        <h4><b>Abstract:</b>
          <font color="#FFFFFF"><img src="pics/prof_taisuke_boku.jpeg" border="1" align="right" class="right" width="200"/></font>
          In the Exa-scale era, one of the most important and tough problems is
          how to enhance the sustained performance against the limited power
          budget. Traditional multi- or many-core general CPUs are still popular
          for easy programming and porting of general applications. However, it
          is getting to face to the limit by semiconductor technology limit,
          memory capacity per core, network bandwidth, etc.

          GPU represents the attached accelerator solution in heterogeneous
          computing thanks to its high peak performance ratio to power
          consumption, and moreover, recent progress on AI applications such as
          TensorFlow ready NVIDIA GPUs. However, GPU's extremely high
          performance is provided by wide width of data parallel computation
          both in instruction level and core/thread level which requires
          thousands of SIMD operations simultaneously executed. Many of success
          stories on GPU acceleration depend on their simple parallel execution
          and quite low rate of exception (if statements) handling. Another
          problem is the interconnection network which relies on CPU-bundle high
          performance network such as InfiniBand.

          In our research team has been focusing on the FPGA computation, which
          is one of the hot topics of new type of accelerators for HPC, however
          it is quite difficult to achieve a comparable performance with GPU
          especially for SIMD style applications. So, we think that a new
          generation of accelerated computing supported by multiple
          heterogeneous accelerator platform including several types of ones
          together on computation node. The first target is a combination of GPU
          and FPGA to provide 360-degree solution with SIMD and pipelined
          parallelism depending on the characteristics of each computation part
          of a large application. In this talk, I will introduce the current
          status of our Multi-Hetero Accelerated System running on University of
          Tsukuba, its hardware and software development, and real application
          with preliminary performance evaluation.

		<h4><b>Bio:</b>
          Taisuke Boku received Master and PhD degrees from Department
          of Electrical Engineering at Keio University. After his career as
          assistant professor in Department of Physics at Keio University, he
          joined to Center for Computational Sciences (former Center for
          Computational Physics) at University of Tsukuba where he is currently
          the director and the HPC division leader.
          He has been working there more than 25
          years for HPC system architecture, system software, and performance
          evaluation on various scientific applications. In these years, he has
          been playing the central role of system development on CP-PACS (ranked
          as number one in TOP500 in 1996), FIRST, PACS-CS and HA-PACS as the
          representative supercomputers in Japan. He is currently the Director
          of Center for Computational Sciences, University of Tsukuba, and the
          Vice Director of JCAHPC (Joint Center for Advanced HPC) which is a
          joint organization by University of Tsukuba and the University of
          Tokyo to operate the largest KNL base cluster in Japan, Oakforest-PACS
          (25PFLOPS peak performance). He is also the Chair of HPCI Resource
          Management and Service Committee for all supercomputer resource
          utilization under MEXT HPCI program. He is a member of system
          architecture working group of Post-K Computer development. He received
          ACM Gordon Bell Prize in 2011.
	  </div>

    <div class="midBox1">
        <!-- <h1>Tentative Program (Time in <span style="background-color: #ff4d00">CDT,</span> IPDPS New Orleans)</h1> -->
        <h1>Opening Statement</h1>
        <h3>1:25 pm - 1:35 pm CDT</h3>
        <h3>Min Si, Argonne National Laboratory </h>
        
        <h1>Session One: GPU computing</h1>
        <h3>1:35 pm - 3:15 pm CDT</h3>
        <!-- <h4>Note: Workshop connection information will be announced to registered attendees by email.</h4> -->
        <h3>Session Chair: Simon Garcia de Gonzalo, Barcelona Supercomputing Center</h3>
        <ul>
          <li>
            <b>Towards automated kernel selection in machine learning systems: A SYCL case study</b><br/>
            John Lawson
          </li>
          <li>
            <b>Unified data movement for offloading Charm++ applications</b><br/>
            Matthias Diener,  Laxmikant Kale
          </li>
          <li>
            <b>Population Count on Intel CPU, GPU, and FPGA</b> <br/>
            Zheming Jin,  Hal Finkel
          </li>
          <li>
            <b>SPHYNX: Spectral Partitioning for HYbrid aNd aXelerator-enabled systems</b><br/>
            Seher Acer,  Erik G. Boman,  Sivasankaran Rajamanickam
          </li>
        </ul>

        <!-- <div class="midBox1"> -->
        <h2>Break 3:15 - 3:50 pm CDT</h2>
        <!-- </div> -->
        <!-- <h4><b></h4> -->

        <h1>Session Two: FPGAs</h1>
        <h3>3:50 pm - 5:30 pm CDT</h3>
        <h3>Session Chair: Lena Oden, FernUniversität in Hagen</h3>
        <ul>
          <li>
            <b>Understanding the Performance of Elementary Numerical Linear Algebra Kernels in FPGAs</b><br/>
            Federico Favaro, Juan Oliver, Ernesto Dufrechou, Pablo Ezzatti
          </li>
          <li>
            <b>Scalability of Sparse Matrix Dense Vector Multiply (SpMV) on a Migrating Thread Architecture</b><br/>
            Brian A. Page, Peter M. Kogge
          </li>
          <li>
            <b>In-depth Optimization with the OpenACC-to-FPGA Framework on an Arria X FPGA</b><br/>
            Jacob Lambert, Seyong Lee,  Jeffrey Vetter, Allen Malony
          </li>
          <li>
            <b>Performance Evaluation of Pipelined Communication Combined with Computation in OpenCL Programming on FPGA</b><br/>
            Norihisa Fujita, Ryohei Kobayashi, Yoshiki Yamaguchi, Tomohiro Ueno, Kentaro Sano, Taisuke Boku
          </li>
        </ul>

        
    </div>
    <!-- <div class="midBox1">
        <h1>Best Paper Award</h1>
        <h3>TBD</h3>
    </div> -->
    
	  <!-- <div class="midBox1">
      	<h1>Closing Remarks</h1>
      	<h3>TBD</h3>
    </div> -->
	  
    </div>
    	<?php include('footer.php'); ?>
    </div>
  </body>
</html>
