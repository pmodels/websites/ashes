<html>
  <head>
    <title>AsHES Workshop</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">

      <?php include('header.php'); ?>
      <div id="sub-frame">

        <h1>Journal Special Issue Archive</h1>
		<div id="description">
		  The special issues published in the past are listed below.
		  <!-- 2018 special issue -->
		  <p><a href = "https://www.sciencedirect.com/journal/parallel-computing/special-issue/10QS41TGH7Z">ASHES 2018 Sepcial Issue</a> (Edited by Antonio J. Peña, Min Si)
		  </p> 

		  <!-- 2017 special issue -->
		  <p><a href = "https://www.sciencedirect.com/science/article/pii/S0167819118301844">ASHES 2017 Sepcial Issue</a> (Edited by Sunita Chandrasekaran and Antonio J. Peña)
		  </p>

		  <!-- 2016 special issue -->
		  <p><a href = "https://www.sciencedirect.com/science/article/pii/S0167819117301084">ASHES 2016 Sepcial Issue</a> (Edited by Sunita Chandrasekaran and Antonio J. Peña)
		  </p>

		  <!-- 2013 special issue -->
		  <p><a href = "https://journals.sagepub.com/doi/full/10.1177/1094342014527657">ASHES 2013 Sepcial Issue</a> (Edited by Jiayuan Meng and Toshio Endo)
		  </p>

		  <!-- 2011 special issue -->
		  <p><a href = "https://journals.sagepub.com/doi/full/10.1177/1094342012442457">CACHES 2011 Sepcial Issue</a> (Edited by Pavan Balaji and Jiayuan Meng)
		  </p>
		  
		
      </div>

      <?php include('footer.php'); ?>

    </div>
  </body>
</html>
