=====================================================================
                 The First International Workshop on 
    Characterizing Applications for Heterogeneous Exascale Systems   
                           (CACHES 2011)

                  held in conjunction with the
     25th International Conference on Supercomputing (ICS 2011)
                  Tucson, Arizona, June 4, 2011

          http://www.mcs.anl.gov/events/workshops/caches/2011
=====================================================================

CALL FOR PAPERS
---------------
As we look forward to the exascale era, heterogeneous parallel machines 
with accelerators, such as GPUs, FPGAs, and upcoming on-chip accelerator 
cores, are expected to play a massive role in architecting the largest 
systems in the world. While there is significant interest in accelerator-
based architectures, much of this interest is an artifact of the hype 
associated with them. However, without understanding the behavior of
detailed kernels or even entire applications on these architectures,
it is unclear how future systems would be designed based on these
architectures.

For accelerator-based heterogeneous systems to truly be a successful High
Performance Computing platform, it is important that we obtain a
complete picture of HPC applications and learn the opportunities and
challenges these architectures raise. This workshop aims at providing
a platform where the characteristics of computational kernels and
applications, and how different software stacks impact them, are
presented to the research community to guide future accelerator-based HPC
system designs.

SUBMISSIONS
-----------
We solicit papers on all aspects of HPC application studies, especially
those that involve accelerators such as GPUs, FPGAs, etc. The topics 
include (but are not limited to):

 * Categorizing/characterizing of HPC applications and kernels with
   respect to patterns in computation structure, communication, cache
   accesses, memory, I/O, and file accesses.

 * Evaluating the importance of individual kernels within an entire
   application.

 * Modeling for applications running on accelerator-based heterogeneous 
   HPC systems.

 * Implication of workload characterization in heterogeneous design issues.

 * Benchmarking of applications, kernels or software stacks and tools
   supporting applications.

Submitted manuscripts are expected to be within 8 pages and should 
be formatted using the ACM SIG Proceedings alternate style 
(single-spaced, double-column format). All papers will be
subjected to blind reviews. The submission site is
http://www.easychair.org/conferences/?conf=caches2011


SPECIAL JOURNAL ISSUE
---------------------
The workshop proceedings will be published electronically with the ICS
proceedings via the ACM Digital Library. Selected papers will be
published in the Journal of High Performance Computing Applications
(JHPCA) with a special issue titled "Applications for the
Heterogeneous Computing Era".

IMPORTANT DATES
---------------
 * Submission deadline extended to:      March 17th,   2011, 11:59 PDT
 * Author Notification:      April 7th,   2011
 * Camera-ready deadline:    April 21th,  2011


WORKSHOP CHAIRS
---------------
Jiayuan Meng, Argonne National Laboratory
Pavan Balaji, Argonne National Laboratory

PUBLICITY AND CYBER CHAIR
-------------------------
Gregory Diamos, Georgia Institute of Technology

PROGRAM COMMITTEE
-----------------
David Bader             Georgia Institute of Technology, USA
Taisuke Boku		Tsukuba University, Japan
Surendra Byna           Lawrence Berkeley National Laboratory, USA
Jonathan Cohen		NVIDIA Corporation, USA
Torsten Hoefler         National Center for Supercomputing Applications, USA 
Hyesoon Kim		Georgia Institute of Technology, USA
Sriram Krishnamoorthy   Pacific Northwest National Laboratory, USA
Heshan Lin              Virginia Polytechnic Institute and State University, USA
Yutong Lu		National University of Defense Technologies, China
Dimitris Nikolopoulos   University of Crete/FORTH, Greece
John Owens              University of California, Davis, USA
P. Sadayappan		Ohio State University, USA
Allan Snavely		University of California, San Diego
Bronis de Supinski      Lawrence Livermore National Laboratory, USA
Jeffrey Vetter	 	Oak Ridge National Laboratory, USA
Thomas Wenisch          University of Michigan, USA
Yunquan Zhang		Chinese Academy of Sciences, China

WEBSITE
-------
http://www.mcs.anl.gov/events/workshops/caches/2011

CONTACT
-------
If you have any questions, please contact us at
caches-chairs@mcs.anl.gov

