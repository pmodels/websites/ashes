
<div id=heading" style="background-image: url(pics/head_bkground.png)">
<br>

<div id="title">
The Second International Workshop on
<br>Accelerators and Hybrid Exascale Systems (AsHES)<br>
</div>

<div id="subtitle">Join us on May 25th, 2012 in Shanghai, China</div>

<div id="subsubtitle">to be held in conjunction
  with<br><a href="http://www.ipdps.org">IPDPS 2012: IEEE
  International Parallel and Distributed Processing
  Symposium</a></div>

<br>

<div id="topnavigation">
  <ul>
	<li><a href="index.php" class="rborder">HOME</a></li>
	<li><a href="organizers.php" class="rborder lborder">ORGANIZERS</a></li>
	<li><a href="cfp.php" class="rborder lborder">CALL FOR PAPERS</a></li>
	<li><a href="reg.php" class="rborder lborder">REGISTRATION</a></li>
	<li><a href="program.php" class="rborder lborder">PROGRAM</a></li>
	<li><a href="submission.php" class="rborder lborder">SUBMISSION</a></li>
	<li><a href="contact.php" class="lborder">CONTACT US</a></li>
  </ul>
</div>
</div>

