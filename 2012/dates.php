<h1>Important Dates</h1>
<div id="description">
  <p><b>Paper Submission:</b> <strike>Dec. 7th, 2011</strike> <b>Extended to</b> <strong style="color: red;">Dec. 19th, 2011, 11:59 pm PST</strong></p>
  <p><b>Author Notification:</b> Jan. 30th, 2012</p>
  <p><b>Camera-Ready Deadline:</b> Feb. 19th, 2012</p>
  <p><b>Workshop:</b> May, 2012</p>
</div>

