<h1>Journal Special Issue</h1>
<div id="description">

  <p>The best papers of AsHES 2012 will be included in a <bold>
  Special Issue on Applications for the Heterogeneous Computing
  Era</bold> of the <a href="http://hpc.sagepub.com/">International
  Journal of High Performance Computing Applications</a>, edited by
    <a href="http://www.mcs.anl.gov/~balaji">Pavan Balaji</a> and
    <a href="http://matsu-www.is.titech.ac.jp/~matsu/">Satoshi
    Matsuoka</a> (<a href = si_archive.php>Link to past Speical Issues</a>).</p>

</div>
