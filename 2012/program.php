<html>
  <head>
    <title>AsHES Workshop</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">

      <?php include('header.php'); ?>
      <div id="sub-frame">
	<div>
	  <img src="pics/onsite/bar2.JPG" height="180" alt="" />
	</div>

      <div class="midBox1">
	<h2>Opening Session</h2>

	<h3>8:00am - 9:30am</h3>
	<ul>
		<li class="col2 coltopper3"><b>Opening Remarks</b></li>
        Pavan Balaji, Jiayuan Meng, Yunquan Zhang and Satoshi Matsuoka<br>

        <br>

		<li class="col2">
		      Keynote: <b>An Ecosystem for the New HPC: Heterogeneous Parallel Computing</b>
                      </br> Prof. Wu-chun Feng, Virginia Tech
                      </br>
	  	      <img src="pics/onsite/Wu.JPG" height="180" alt="" />
                      <a href="slides/Feng-AsHES12-keynote.pdf" style="color:#fdf5e0" >Slides</a>
                      </br><i>Abstract:</i> 
                      </br>With processor core counts doubling every 18-24 months and penetrating all markets from high-end servers in supercomputers to desktops and laptops down to even mobile phones, we sit at the dawn of a world of ubiquitous parallelism, one where extracting performance via parallelism is paramount.  That is, the "free lunch" to better performance, where programmers could rely on substantial increases in single-threaded performance to improve software, is over.  The burden falls on developers to exploit parallel hardware for performance gains.  But how do we lower the cost of extracting such parallelism, particularly in the face of the increasing heterogeneity of processing cores?  To address this issue, this talk will present a vision for an ecosystem for delivering accessible and personalized supercomputing to the masses, one with a heterogeneity of (hardware) processing cores on a die or in a package, coupled with enabling software that tunes the parameters of the processing cores with respect to performance, power, and portability via a benchmark suite of computational dwarfs and applications.
                </li>

        <br>
	</ul>
      </div>

<div class="midBox2">
	<h2>Break</h2>
	<h3>10:00am-10:30am</h3>
</div>

<div class="midBox1">
	<h2>Session 1: Modeling and Optimization</h2>
	<h3>10:30am - 12:00pm</h3>
               Session Chair: Prof. Wenguang Cheng, Tsinghua Universty, China
	<div>
	  <img src="pics/onsite/talk1.JPG" height="180" alt="" />
	  <img src="pics/onsite/talk2.JPG" height="180" alt="" />
	  <img src="pics/onsite/talk3.JPG" height="180" alt="" />
	</div>
	<ul>
		<li class="col1">
			<b>Modeling and Predicting Performance of High Performance Computing Applications on Hardware Accelerators.</b> </br>
                            Mitesh Meswani, Laura Carrington, Didem Unat, Allan Snavely, Scott Baden and Stephen Poole
                      <a href="slides/Meswani_AsHES12.pdf"  style="color:#fdf5e0"  >Slides</a>
                </li>
		<li class="col1">
			<b>Efficient Intranode Communication in GPU-Accelerated Systems.</b> </br>
			    Feng Ji, Ashwin Aji, James Dinan, Darius Buntinas, Pavan Balaji, Wu-Chun Feng and Xiaosong Ma
                      <a href="slides/Ji_AsHES12.pdf" style="color:#fdf5e0" >Slides</a>
                </li>
		<li class="col1">
			<b>Optimizing MPI Communication on Multi-GPU Systems using CUDA Inter-Process Communication.</b> </br>
			    Sreeram Potluri, Hao Wang, Devendar Bureddy, Ashish Singh, Carlos Rosales and Dhabaleswar Panda
                      <a href="slides/Potluri_AsHES12.pdf" style="color:#fdf5e0" >Slides</a>
                </li>

        <br>
	</ul>
</div>

<div class="midBox2">
		<h2>Lunch</h2>
		<h3>12:00pm - 1:30pm</h3>
</div>

<div class="midBox1">
	<h2>Session 2: Programming Models</h2>
	<h3>1:30pm - 3:00pm</h3>
               Session Chair: Dr. Vinod Tipparaju, AMD, USA</p>
	<div>
	  <img src="pics/onsite/talk4.JPG" height="180" alt="" />
	  <img src="pics/onsite/talk5.JPG" height="180" alt="" />
	  <img src="pics/onsite/talk6.JPG" height="180" alt="" />
	</div>
	<ul>
		<li class="col1">
			<b>Towards High-Level Programming of Multi-GPU Systems Using the SkelCL Library.</b> </br>
			Michel Steuwer, Philipp Kegel and Sergei Gorlatch
                      <a href="slides/Steuwer_AsHES12.pdf" style="color:#fdf5e0" >Slides</a>
                </li>
		<li class="col1">
			<b>Scaling Data-Intensive Applications on Heterogeneous Platforms with Accelerators.</b> </br>
			Ana Balevic and Bart Kienhuis
                      <a href="slides/Balevic_AsHES12.pdf" style="color:#fdf5e0" >Slides</a>
                </li>
		<li class="col1">
			<b>An Analysis of Multicore Specific Optimization in MPI Implementations.</b> </br>
			Pengqi Cheng and Yan Gu
                      <a href="slides/Cheng_AsHES12.pdf" style="color:#fdf5e0" >Slides</a>
                </li>

        <br>
	</ul>
</div>

<div class="midBox2">
	<h2>Break</h2>
	<h3>3:00pm-3:30pm</h3>
</div>

<div class="midBox1">
	<h2>Session 3: Accelerated Applications</h2>
	<h3>3:30pm - 4:30pm</h3>
               Session Chair: Prof. Toshio ENDO, Tokyo Institute of Technology, Japan
	<div>
	  <img src="pics/onsite/talk7.JPG" height="180" alt="" />
	  <img src="pics/onsite/talk8.JPG" height="180" alt="" />
	</div>
	<ul>
		<li class="col1">
			<b>Parallelizing the Hamiltonian Computation in DQMC Simulations:  Checkerboard Method for Sparse Matrix Exponentials on Multicore and GPU.</b> </br>
			Che-Rung Lee, Zhi-Hung Chen and Quey-Liang Kao
                      <a href="slides/Lee_AsHES12.pdf" style="color:#fdf5e0" >Slides</a>
                </li>
		<li class="col1">
			<b>Parallel Multi-Temporal Remote Sensing Image Change Detection on GPU.</b> </br>
			Huming Zhu, Yu Cao, Zhiqiang Zhou and Maoguo Gong
                      <a href="slides/Zhu_AsHES12.pdf" style="color:#fdf5e0" >Slides</a>
                </li>
		<li class="col1">
			<b>Implementing High-performance Intensity Model with Blur Effect on GPUs for Large-scale Star Image Simulation.</b> </br>
			Chao Li and Yunquan Zhang. Presented by Liang Yuan
                      <a href="slides/Li_AsHES12.pdf" style="color:#fdf5e0" >Slides</a>
                </li>


        <br>
	</ul>
</div>


<?php include('footer.php'); ?>
</body>
</html>
