<html>
  <head>
    <title>AsHES Workshop</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">

      <?php include('header.php'); ?>
      <div id="sub-frame">

        <h1>Scope</h1>
        <div id="description">
          	<ul>
            	<li>Strategies for programming heterogeneous systems using high-level models such as OpenMP, OpenACC, low-level models such as OpenCL, CUDA; </li>
            	<li>Methods and tools to tackle challenges in scientific computing at extreme scale;</li>
            	<li>Strategies for application behavior characterization and performance optimization for accelerators;</li>
            	<li>Techniques for optimizing kernels for execution on GPGPU, Intel® Xeon Phi™, and future heterogeneous platforms;</li>
            	<li>Models of application performance on heterogeneous and accelerated HPC systems;</li>
            	<li>Compiler Optimizations and tuning heterogeneous systems including parallelization, loop transformation, locality optimizations, Vectorization;</li>
            	<li>Implications of workload characterization in heterogeneous and accelerated architecture design;</li>
		<li>Benchmarking and performance evaluation for accelerators;</li>
		<li>Tools and techniques to address both performance and correctness to assist application development for accelerators and heterogeneous processors;</li>
		<li>System software techniques to abstract application domain-specific functionalities for accelerators;</li>
          	</ul>
        	</div>
	

				
	<h1>Proceedings</h1>
        <div id="description">
          <!--<p>The proceedings of this workshop will be published
          electronically together with with IPDPS proceedings via the
          IEEE Xplore Digital Library.</p>-->
         <p> Proceedings of the workshops are published by IEEE CPS; they are
             distributed at the conference and are submitted for inclusion in the
             IEEE Xplore Digital Library after the conference.</p>
        </div>

       <?php include('instructions.php'); ?>
        <?php include('journal.php'); ?>
        <?php include('dates.php'); ?>
				
      </div>

      <?php include('footer.php'); ?>

    </div>
  </body>
</html>
