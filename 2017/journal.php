<h1>Journal Special Issue</h1>
<div id="description">
<p>The best papers of AsHES 2017 will be invited to a <b>Special Issue on Topics on Heterogeneous Computing</b> of the <a href="http://www.journals.elsevier.com/parallel-computing/">Elsevier International Journal on Parallel Computing (PARCO)</a><!--, edited by Sunita Chandrasekaran and Antonio J. Peña. This special issue is dedicated for the papers accepted in the AsHES workshop. The submission to this special issue is by invitation only. We are finalizing the details right now. Please continue to check our website for further details.--> (<a href = si_archive.php>Link to Speical Issues List</a>)</p>


</div>
