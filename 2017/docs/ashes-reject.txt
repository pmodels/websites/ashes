Dear [*FIRST-NAME*], 

The AsHES '17 program committee regrets to inform you that your paper entitled

[*TITLE*]

has been rejected.  Feedback from the reviewers is attached.  We hope that the reviewer comments will be helpful in further developing your work.

This decision is the result of a thorough review process.  This year AsHES received many excellent submissions and a limited number of papers could be selected for the program.

We hope that you will be able to attend IEEE IPDPS '17 and the AsHES workshop.  Thank you for submitting your work to AsHES and we look forward to your participation in future events.

Best Regards,

Antonio J. Pena and Sangmin Seo
AsHES'17 Program Co-chairs
