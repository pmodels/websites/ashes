<html>
  <head>
    <title>AsHES Workshop</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">

      <?php include('header.php'); ?>
      <div id="sub-frame">

		<h1>Contact Address</h1>
        <div id="description">
          <p>Please send any queries about the AsHES workshop
          to <a href="mailto:ashes@mcs.anl.gov">ashes@mcs.anl.gov</a></p>
        </div>
				
		<h1>Mailing Lists</h1>
        <div id="description">
          <p>To hear announcements about AsHES, please subscribe to the
          announcement mailing
          list <a href="http://lists.mcs.anl.gov/mailman/listinfo/hpc-announce">here</a>.</p>
        </div>

      </div>

      <?php include('footer.php'); ?>

    </div>
  </body>
</html>
