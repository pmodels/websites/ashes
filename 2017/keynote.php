<html>
  <head>
    <title>AsHES Workshop</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">

      <?php include('header.php'); ?>
      <div id="sub-frame">

	  <h1>Keynote</h1>
	  <h4>><a href="http://hpcgarage.org/ashes2013/"><b>What first principles of algorithms and architectures says about heterogeneity</b></a></h4>
	  <h4>Prof. Richard Vuduc, Georgia Institute of Technology</h4>
	  <h4>
	  <i>Abstract:</i>
	  <p>In this talk, we will "pop up a level" and ask whether first
	  principles of algorithm and architecture design can tell us anything
	  about heterogeneity. In particular, I'll posit "strawman" cost models
	  that connect properties of an algorithm with physical properties of
	  machines, like energy, power, and area. I will then explain what these
	  models imply. For instance, I'll use them to evaluate the time- and
	  energy-efficiency of heterogeneous designs as well as for predicting
	  the benefits of other forms of heterogeneity, such as "communication"
	  heterogeneity. And although I'll show a bunch of formulas, calculate
	  things, and plot them, I stress that this talk is about ideas, rather
	  than a well-developed set of results. As such, your questions, healthy
	  skepticism, (constructive!) feedback, and offers of collaboration may
	  be even more welcome than usual! :-)</p>
	  <p>This work is joint with current and former students: Jee Whan Choi,
	  Aparna Chandramowlishwaran (MIT), Marat Dukhan, and Kenneth
	  Czechowski. The talk will preview two papers (one lead by Choi, the 
			  other by Czechowski) to appear at the main IPDPS'13 conference later
	  in the week.</p>
	  <i>Bio:</i>
	  <p>Richard (Rich) Vuduc is an Associate Professor at Georgia Tech
	  in the School of Computational Science and Engineering. His research
	  lab, the HPC Garage (hpcgarage.org), is interested in
	  all-things-high-performance-computing, with an emphasis on parallel
	  algorithms, performance analysis, and performance tuning. He is a
	  member of the DARPA Computer Science Study Panel, recipient of the NSF
	  CAREER Award, and co-recipient of the Gordon Bell Prize (2010). His
	  lab's work has received a number of best paper nominations and awards,
	  including most recently the 2012 Best Paper Award from the SIAM
	  Conference on Data Mining.</p>
	  </h4>

      </div>

      <?php include('footer.php'); ?>

    </div>
  </body>
</html>
