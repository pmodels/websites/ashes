<html>
  <head>
    <title>AsHES Workshop: AsHES 2017 Preliminary Program</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">
      <?php include('header.php'); ?>

<div id="sub-frame">
	<div class="midBox1">
	<h1>Opening remarks</h1>
	<h3>8:45 - 9:00 am<br />
	AsHES Program Co-Chair: Antonio J. Pe&ntildea, Barcelona Supercomputing Center, Spain</h3>
	</div>

<div class="midBox1">
<h1>Keynote by Tim Mattson</h1>
<h3>9:00 - 10:00 am</h3>
<h4>"Accelerators and Exascale Systems: A programmer’s perspective"</h4>
<h4><b>Abstract:</b>
<!--<img src="pics/michela.png" border="3" align="right" class="right"/>-->
Life as an HPC researcher used to be so easy.  MPI between nodes and OpenMP on a node and you were done.  GPUs came along and complicated things, but OpenCL and later the OpenMP device constructs gave us a portable way to write software for nodes composed of CPUs and GPUs.   It was awkward but at least you could still build a base of software around portable, open standards.
<br />
Now with FPGAs and fixed function accelerators added to the mix, the ability to maintain an application from a single code base has become prohibitively difficult.  On top of this, there is no consensus on how we will scale between nodes on an exascale system.  Sure, there is always MPI but it’s not clear how the reliability needs of an exascale system will be addressed by MPI.  Many new programming models have been suggested, but none of them have proven themselves to be “ready for prime time”.  Things are quite confusing.  
<br />
In this talk we will explore these issues from a programmer’s perspective.  We will present a path forward and a workable solution, but in all honesty, there are still too many uncertainties to know the safest approach for an application programmer to take.  Which ultimately is the real goal of this talk.  We need to get the issues out on the table so we can effectively push the people who build our hardware to work with us to create software solutions that support the needs of our exascale-class applications.  
</h4>

<h4><b>Bio:</b>
Tim Mattson is an old-fashioned application programmer.  Starting in the sciences (Ph.D. Chemistry, UCSC, 1985) he progressed to high performance computing where he worked with programming models (Linda, MPI, OpenMP, OpenCL, MPI+Fenix) and contributed to influential hardware/software co-design efforts (The first teraflop computing in 1997 and ten years later, the first teraflop chip).  Tim is the PI for Intel’s Science and Technology center for Big Data.  Among the projects at the center, he has been directly involved with an innovative array storage engine (TileDB), Linear algebra primitives for Graph algorithms (GraphBLAS), and Polystore Database management systems (BigDAWG).  
</div>

<div class="midBox1">
<h2>Break 10:00 - 10:30 am</h2>
</div>

<div class="midBox1">
<h1>Session 1: Programming Models and Runtime Systems</h1>
<h3>10:30 am - 12:00 pm<br />
Session Chair: CJ Newburn, NVIDIA, USA </h3>
<ul>
    <li>
    <b>Implementing the OpenACC Data Model</b><br />
Michael Wolfe, Seyong Lee, Jungwon Kim, Xiaonan Tian, Rengan Xu, Sunita Chandrasekaran and Barbara Chapman    
    </li>
    <li>
    <b>Exploring Translation of OpenMP to OpenACC 2.5: Lessons Learned</b><br />Sergio Pino, Lori Pollock and Sunita Chandrasekaran
    </li>
    <li>
    <b>Exploring the Performance Benefit of Hybrid Memory System on HPC Environments</b><br />
    Ivy Bo Peng, Roberto Gioiosa, Stefano Markidis, Gokcen Kestor, Pietro Cicotti and Erwin Laure  
    </li>
</ul>
</div>

<div class="midBox1">
<h2>Lunch 12:00 - 1:30 pm</h2>
</div>

<div class="midBox1">
<h1>Session 2: Algorithms</h1>
<h3>1:30 - 3:00 pm<br />
Session Chair: Piotr Luszczek, The University of Tennessee, USA </h3>
<ul>
    <li>
    <fli>
    <b>Performance-Portable Sparse Matrix-Matrix Multiplication for Many-Core Architectures</b><br />
    Mehmet Deveci, Christian Trott and Siva Rajamanickam
    </li>
    <li>
    <b>Time and Energy to Solution Evaluation for the Three-Point Angular Correlation Function</b><br />
    Antonio G&oacutemez-Iglesias and Miguel C&aacuterdenas-Montes
    </li>
    <li>
    <b>Auto-Tuning Strategies for Parallelizing Sparse Matrix-Vector (SpMV) Multiplication on Multi- and Many-Core Processors</b><br />
    Kaixi Hou, Wu-Chun Feng and Shuai Che
    </li>
</ul>
</div>

<div class="midBox1">
<h2>Break 3:00 - 3:30 pm</h2>
</div>

<div class="midBox1">
<h1>Session 3: Scheduling and Architectures</h1>
<h3>3:30 - 5:00 pm<br />
Session Chair: Antonio G&oacutemez, Texas Advanced Computing Center, USA
</h3>
<ul>
    <li>
    <b>A Pluggable Framework for Composable HPC Scheduling Libraries</b><br />
     	Max Grossman, Vivek Kumar, Nick Vrvilo, Zoran Budimlic and Vivek Sarkar
    </li>
    <li>
    <b>Static versus Dynamic Task Scheduling of the LU Factorization on ARM big.LITTLE Architectures</b><br />
     	Sandra Catal&aacuten, Rafael Rodr&iacuteguez-S&aacutenchez, Enrique S. Quintana-Ort&iacute and Jos&eacute R. Herrero
    </li>
    <li>
    <b>Benchmarking Sunway SW26010 Manycore Processor</b><br />
    Zhigeng Xu, James Lin and Satoshi Matsuoka
    </li>

</ul>
</div>

	<div class="midBox1">
	<h1>Closing Remarks</h1>
	<h3>5:00 pm</h3>
	</div>

</div>
	<?php include('footer.php'); ?>
</div>

</body>
</html>
