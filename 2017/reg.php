<html>
  <head>
    <title>AsHES Workshop</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">

      <?php include('header.php'); ?>
      <div id="sub-frame">
        <h1>Workshop Registration</h1>

        <div id="description">
        <p>The AsHES workshop does not have a separate
        registration. All attendees need to register with the main
        conference. Details can be found on the <a href="http://www.ipdps.org">conference website</a>.</p>
        </div>

      </div>
		
      <?php include('footer.php'); ?>

    </div>
  </body>
</html>
