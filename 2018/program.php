<html>
  <head>
    <title>AsHES Workshop: AsHES 2018 Preliminary Program</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">
      <?php include('header.php'); ?>
      <div id="sub-frame">

      <div class="midBox1">
        <h1>Opening Remarks</h1>
        <h3>8:45 - 9:00 am</h3>
      </div>

      <div class="midBox1">
        <h1>Keynote by Dr. Michael Wolfe</h1>
        <h3>9:00 - 10:00 am</h3>
        <h4><b>Will Accelerators and Hybrid Systems Succeed This Time Around?</b></h4>
        <h3>Michael Wolfe, NVIDIA, USA</h3>
        <h4><b>Abstract:</b>
        <font color="#16A085"><img src="pics/Michael_Wolfe.jpg" border="1" align="right" class="right" width="150"/></font>
        Accelerated systems have been around in HPC for 50 years or more.  IBM produced
        the 2938 Array Processor for mainframes in 1969, and Floating Point Systems
        developed the AP-120B array processor for minicomputers in 1976.  Ten years
        ago, the IBM PowerXCell coprocessor was used in Roadrunner, the fastest machine
        in the TOP500 list.  Yet, repeatedly, the improved performance from each
        accelerator was eventually obviated by faster and more capable CPUs.
        <br />
        Now we see GPUs becoming common as compute accelerators, FPGAs achieving some
        success, and other accelerator ASICs being designed for specific applications.
        How do today's accelerators differ from the previous generations?  What will it
        take to make the current move to accelerators successful in the long term?  Is
        there something unique about the jump from petascale to exascale computing that
        begs for accelerators?  How will compilers and languages have to evolve for
        these systems?  We explore these questions and more.

        <h4><b>Bio:</b>
        Michael Wolfe has worked on languages and compilers for parallel computing
        since his days in graduate school at the University of Illinois in 1976.  Along
        the way, he cofounded Kuck and Associates, Inc. (since acquired by Intel),
        tried his hand in academia at the Oregon Graduate Institute (since merged with
        the Oregon Health and Sciences University), and worked on High Performance
        Fortran at PGI (since acquired by STMicroelectronics, and more recently by
        NVIDIA).  He now spends most of his time as the technical lead for a team
        that works to improve the PGI compilers for highly parallel computing, and in
        particular for GPU accelerators.
      </div>

      <div class="midBox1">
        <h2>Break 10:00 - 10:30 am</h2>
      </div>

      <div class="midBox1">
        <h1>Session 1: Runtime Scheduling and Performance Analytics</h1>
        <h3>10:30 am - 12:00 pm<br />Session Chair: Aparna Chandramowlishwaran, University of California, Irvine, USA</h3>
        <ul>
          <li>
            <b>NVIDIA Tensor Core Programmability, Performance &amp; Precision</b><br />
            Stefano Markidis, Steven Wei Der Chien, Erwin Laure, Ivy Bo Peng and Jeffrey S. Vetter
          </li>
          <li>
            <b>Optimizing an Atomics-based Reduction Kernel on OpenCL FPGA Platform</b><br />
            Zheming Jin and Hal Finkel
          </li>
          <li>
            <b>Leveraging Data-Flow Task Parallelism for Locality-Aware Dynamic Scheduling on Heterogeneous Platforms</b><br />
            Osman Seckin Simsek, Andi Drebes, Mikel Lujan and Antoniu Pop
          </li>
        </ul>
      </div>

      <div class="midBox1">
        <h2>Lunch 12:00 - 1:30 pm</h2>
      </div>

      <div class="midBox1">
        <h1>Session 2: Algorithms and Applications</h1>
        <h3>1:30 - 3:00 pm<br />Session Chair: Stefano Markidis, KTH Royal Institute of Technology, Sweden</h3>
        <ul>
          <li>
          <fli>
            <b>Tacho: Memory-Scalable Task Parallel Sparse Cholesky Factorization</b><br />
            Kyungjoo Kim, H. Carter Edwards and Sivasankaran Rajamanickam
          </li>
          <li>
            <b>Sorting Large Datasets with Heterogeneous CPU/GPU Architectures</b><br />
            Michael Gowanlock and Ben Karsin
          </li>
          <li>
            <b>Improving Performance of Genomic Aligners on Intel Xeon Phi-based Architectures</b><br />
            Shaolong Chen and Miquel Senar
          </li>
        </ul>
      </div>

      <div class="midBox1">
        <h2>Break 3:00 - 3:30 pm</h2>
      </div>

      <div class="midBox1">
        <h1>Session 3: Emerging Accelerator Architectures</h1>
        <h3>3:30 - 4:30 pm<br />Session Chair: Jeffrey Young, Georgia Institute of Technology, USA</h3>
        <ul>
          <li>
            <b>An Initial Characterization of the Emu Chick</b><br />
           	Eric Hein, Tom Conte, Jeffrey Young, Srinivas Eswar, Jiajia Li, Patrick Lavin, Richard Vuduc and Jason Riedy
          </li>
          <li>
            <b>Exploring the Vision Processing Unit as Co-processor for Inference</b><br />
           	Sergio Rivas-Gomez, Stefano Markidis, Antonio J. Pe&ntilde;a, David Moloney and Erwin Laure
          </li>
        </ul>
      </div>

      <div class="midBox1">
      	<h1>Closing Remarks</h1>
      	<h3>4:30 pm</h3>
      	</div>
      </div>

    	<?php include('footer.php'); ?>
    </div>
  </body>
</html>
