<html>
  <head>
    <title>AsHES Workshop</title>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <link href="style/general.css" rel="stylesheet" type="text/css">
  </head>

  <body>
    <div id="main-frame">

      <?php include('header.php'); ?>
      <div id="sub-frame">

        <h1>DESCRIPTION OF THE WORKSHOP</h1>
        <div id="description">
          <img src="pics/vancouver.jpg" border="1" width="300" align="left"/>

          <p> Current and emerging systems are deployed with heterogeneous architectures and accelerators of more than one type e.g. GPGPU, Intel&reg; Xeon Phi&trade;, FPGA along with hybrid processors of both lightweight and heavyweight cores (e.g APU, big.LITTLE). Such architectures also comprise of hybrid memory systems equipped with stacked/hierarchical memory and non-volatile memory in addition to regular DRAM. Programming such a system can be a real challenge along with locality, scheduling, load balancing, concurrency and so on.</p>

          <p> This workshop focuses on understanding the implications of accelerators and heterogeneous designs on the hardware systems, porting applications, performing compiler optimizations, and developing programming environments for current and emerging systems. It seeks to ground accelerator research through studies of application kernels or whole applications on such systems, as well as tools and libraries that improve the performance and productivity of applications on these systems.</p>

          <p> The goal of this workshop is to bring together researchers and practitioners who are involved in application studies for accelerators and other heterogeneous systems, to learn the opportunities and challenges in future design trends for HPC applications and systems.</p>

          <br>
        </div>
        <?php include('dates.php'); ?>
        <br/>
        <?php include('journal.php'); ?>
        <!-- <br/> -->

        <h1>Previous Workshops</h1>
        <div id="description">
          <p><a href="http://www.mcs.anl.gov/events/workshops/ashes/2017">ASHES 2017</a> in Orlando, USA</p>
          <p><a href="http://www.mcs.anl.gov/events/workshops/ashes/2016">ASHES 2016</a> in Chicago, USA</p>
          <p><a href="http://www.mcs.anl.gov/events/workshops/ashes/2015">ASHES 2015</a>,
             <a href="http://web.cs.uh.edu/~hpctools/plc2015/">PLC 2015</a> in Hyderabad, India</p>
          <p><a href="http://www.mcs.anl.gov/events/workshops/ashes/2014">ASHES 2014</a>,
             <a href="http://web.cs.uh.edu/~hpctools/plc2014/">PLC 2014</a> in Phoenix, USA</p>
          <p><a href="http://www.mcs.anl.gov/events/workshops/ashes/2013">ASHES 2013</a>,
             <a href="http://web.cs.uh.edu/~hpctools/plc2013/">PLC 2013</a> in Boston, USA</p>
          <p><a href="http://www.mcs.anl.gov/events/workshops/ashes/2012">ASHES 2012</a> in Shanghai, China</p>
          <p><a href="http://www.mcs.anl.gov/events/workshops/ashes/2011">CACHES 2011</a> in Tucson, Arizona</p>
        </div>

      </div>
      <?php include('footer.php'); ?>

    </div>
  </body>
</html>
